#!coding:utf8
from driver import IOTOSDriverI
import json, time, random

# 继承官方驱动类（ZMIotDriverI）
from library.exception import DataNotExistError


class AdLiftDriver(IOTOSDriverI):

    # 1、通信初始化
    def InitComm(self, attrs):
        self.online(True)
        self.critical("deviceId=" + self.sysId)
        self.debug("deviceId=" + self.sysId)
        self.pauseCollect = False
        # self.collectingOneCircle = True

    # 2、采集
    def Collecting(self, dataId):
        '''*************************************************
        TODO
        **************************************************'''
        try:
            # 通过数据点ID获取数据点属性字典
            data_attr = self.data2attrs[dataId]
            value_type = data_attr["valuetype"]
            if value_type == u'BOOL':
                new_value = random.randint(0, 1)
            elif value_type == u'INT':
                new_value = random.randint(int(data_attr[u'minvalue']), int(data_attr[u'maxvalue']))
            elif value_type == u'FLOAT':
                new_value = random.uniform(float(data_attr[u'minvalue']), float(data_attr[u'maxvalue']))
            else:
                new_value = time.time()
            return (new_value, )
        except DataNotExistError as e:
            self.error(e)
            return None

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})