#!coding:utf8
import json
import winsound
import sys

sys.path.append("..")
from driver import *

import base64
import serial

class Driver(IOTOSDriverI):

	data2serial = {}

	class SerialRcvThread(threading.Thread):
		def __init__(self, dr,cfg):
			threading.Thread.__init__(self)
			self.driver = dr
			self.serial = serial.Serial(port = cfg['port'],
										baudrate = cfg['baudrate'],
										bytesize = cfg['byteSize'],
										parity = cfg['parity'],
										stopbits = cfg['stopbits'],
										xonxoff = cfg['xonxoff']
          )

		lock = threading.Lock()
		def run(self):
			while True:
				dataHex = bytes()
				dataStr = ''
				n = self.serial.inWaiting()
				if n:
					dataHex += self.serial.read(n)
					try:
						dataStr = str(dataHex).encode('utf-8')
					except Exception as e:
						dataStr = dataHex.decode('gbk').encode('utf-8')
				if dataStr != '':  # 本地串口下发的数据，转发到路由设备代理
					try:
						# print 'recv:', dataStr
						self.driver.setValue('接收', dataStr)
					except Exception as e:
						traceback.print_exc()
						print('send failed:', e.message)
						try:
							self.lock.acquire()
							self.InitComm(None)
						finally:
							self.lock.release()

		def send(self,value):
			return self.serial.write(bytes(value.encode('utf-8')))


	#1、通信初始化
	""" tips 240606，设备驱动：典型配置
	{
		"parentid": null,
		"driver": "dr_serialPort",
		"param": {
			"port":"COM1",
				"baudrate":9600,
				"byteSize": 8,
				"parity" : "N",
				"stopbits": 1,
				"xonxoff":true
		}
	}
	"""
	def InitComm(self,attrs):
		try:           
			self.serialInst = serialtmp = self.SerialRcvThread(self,attrs['config']['param'])
			serialtmp.send(attrs['config']['param']['port'] + ' connected!')
			serialtmp.start()
			self.online(True);
		except Exception as e:
			print(traceback.print_exc())
	# #2、采集
	# def Collecting(self, dataId):
	# 	return "ewd"

	#3、控制
	#事件回调接口，其他操作访问
	def Event_customBroadcast(self, fromUuid, type, data):

		# tabletmp = {}
		# tabletmp[self.sysId] = self.sysAttrs
		# tabletmp[self.sysId]['data'] = self.data2attrs
		# print json.dumps(tabletmp),'\r\n'

		return json.dumps({'code':0, 'msg':'', 'data':''})


	# 事件回调接口，监测点操作访问
	def Event_getData(self, dataId, condition = ''):
		configtmp = self.data2attrs[dataId]['config']

		return json.dumps({'code':0, 'msg':'', 'data':''})


	# 事件回调接口，监测点操作访问
	def Event_setData(self, dataId, value):
		codetmp = 1;
		retmsg = None;
		try:
			print('接收到',value)
			# retmsg = self.serialInst.send(str(value).encode('utf-8'));	#240821，python2
			retmsg = self.serialInst.send(value);	#240821，python3
			codetmp = 0;
		except Exception as e:
			print('错误',e)
			pass
  
		# if self.data2attrs[dataId]['valuetype'] == 'BLOB':
		# 	value = base64.b64decode(value)
		# try:
		# 	retmsg = None
		# 	codetmp = 1
		# 	retmsg = self.data2serial[dataId].send(value)
		# 	if retmsg > 0:
		# 		codetmp = 0
		# 	print self.__class__.__name__, 'Event_setData', self.sysAttrs['name'], self.name(dataId), value
		# except Exception as e:
		# 	print traceback.print_exc()
		# 	print 'serial.send', e.message
		# 	self.InitComm(None)



		return json.dumps({'code':codetmp, 'msg':'', 'data':retmsg})


	# 事件回调接口，监测点操作访问
	def Event_syncPubMsg(self, point, value):
		'''*************************************************

		TODO 

		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})