# coding=utf-8
"""驱动API回调"""
import datetime
import json
import socket
import time
import platform
import requests
import threading
import logging

logger = logging.getLogger(__name__)


class DataSetValuesRequestModel(object):
    """数据点批量上报

    {
      "id": "{网关ID}.{设备ID}",
      "data": {
        "数据点名称": "数据点设置值",
        "数据点名称2": "数据点设置值",
        "数据点名称3": "数据点设置值",
        "数据点名称4": "数据点设置值",
        "数据点名称5": "数据点设置值"
      },
      "time": 1234567890
    }
    """

    id = None  # type: str
    time = None  # type: int
    data = None  # type: dict

    def __init__(self, id, time, data, **kwargs):
        if len(kwargs) > 0:
            logger.warning('more params:%s', kwargs)
        self.id = id
        self.time = time
        self.data = data


class DataSetValueRequestModel(object):
    """数据点单个上报

    {
      "id": "{网关ID}.{设备ID}.{数据点ID}",
      "value": "数据点值", "time": 1234567890
    }
    """

    id = None  # type: str
    time = None  # type: int
    value = None  # type: object

    def __init__(self, id, time, value, **kwargs):
        if len(kwargs) > 0:
            logger.warning('more params:%s', kwargs)
        self.id = id
        self.time = time
        self.value = value


class ApiConfig(object):
    """api接口配置参数"""
    gateway_uuid = None  # type: str
    device_oid = None  # type: str
    host = None  # type: str
    port = None  # type: int
    jwt_token = None  # type: str

    def __init__(self, gateway_uuid, device_oid, host, jwt_token):
        self.gateway_uuid = gateway_uuid
        self.device_oid = device_oid
        self.host = host
        self.port = 13885
        self.jwt_token = jwt_token


class HttpCallBackI(object):
    """http回调接口"""

    def on_setValues(self, data):
        logger.info(data)

    def on_setValue(self, data):
        logger.info(data)


class DriverHttpApi(object):
    """南向接口"""
    __apiConfig = None  # type: ApiConfig
    __apiSession = None  # type: requests.Session
    __ApiCallBackI = None  # type: HttpCallBackI
    __socket = None  # type: socket.socket
    __socketLock = None  # type: threading.Lock

    def __init__(self, apiConfig, callBackI):
        """

        :type apiConfig: ApiConfig
        :type callBackI: HttpCallBackI
        """
        self.__apiConfig = apiConfig
        self.__ApiCallBackI = callBackI
        self.__socketLock = threading.Lock()

        # 1.与注册中心建立联接
        self.__connect_hub()
        # 2.向注册中心报备自己
        t = threading.Thread(target=self.hub_register)
        t.setDaemon(True)
        t.setName('hub_register')
        t.start()

    def set_values(self, request):
        """数据点批量上报"""
        self.__ApiCallBackI.on_setValues(data=request)
        return dict(code=0)

    def set_value(self, request):
        """数据点批量上报"""
        self.__ApiCallBackI.on_setValue(data=request)
        return dict(code=0)

    def __connect_hub(self):
        """联接注册中心，报备自己"""
        while True:
            try:
                # 如果当前系统为Windows,说明为开发调试模式。可以指向本地服务端口
                if platform.system() == 'Windows':
                    host = '127.0.0.1'
                else:
                    host = self.__apiConfig.host
                port = self.__apiConfig.port
                logger.info('%s:%s,Connecting', host, port)
                address = (host, port)
                sk = socket.socket()
                sk.connect(address)
                sk.settimeout(5)
                self.__socket = sk
                logger.info('%s:%s,Connected', host, port)
                break
            except socket.error as ex:
                logger.info(ex)
                time.sleep(1)


    def __publish(self, data):
        return

    def hub_register(self):
        """向注册中心注册自己"""
        heartbeat_time = 0
        rr = self.__hub_register()
        while True:
            try:
                response = self.__socket.recv(10240)  # type: bytes
                logger.info(response)
                response_text = response.decode('UTF-8')
                if len(response_text) == 0:
                    continue
                response_dict = json.loads(response_text)
                reqsponse_data = None
                if 'action' in response_dict:
                    action = response_dict['action']
                    request_dict = response_dict['data']
                    if action == 'setValue':
                        reqsponse_data = self.__ApiCallBackI.on_setValue(data=request_dict)
                    elif action == 'setValues':
                        reqsponse_data = self.__ApiCallBackI.on_setValues(data=request_dict)
                data = dict(code=0, response_time=int(time.time() * 1000), data=reqsponse_data)
                data_text = json.dumps(data)
                data_bytes = data_text.encode('UTF-8')
                self.__socket.send(data_bytes)

                if rr is True:
                    logger.info('hub_register:%s', datetime.datetime.now())
                else:
                    logger.warning('hub_register:%s', datetime.datetime.now())
                time.sleep(10)
                heartbeat_time += 10
            except socket.timeout as ex:
                continue
            except socket.error as ex:
                self.__connect_hub()
            except Exception as ex:
                logger.error("", exc_info=1)
                self.__connect_hub()

    def __hub_register(self):
        """向注册中心注册自己"""
        data = dict(gateway_uuid=self.__apiConfig.gateway_uuid,
                    device_oid=self.__apiConfig.device_oid,
                    jwt_token=self.__apiConfig.jwt_token, action='register')
        response = self._socket_sendRecv(data=data)
        logger.info(response)
        return True

    def _socket_sendRecv(self, data):
        """先发送数据，后读取数据

        :type data: dict
        """
        recv_data = None
        try:
            self.__socketLock.acquire()
            request = json.dumps(data).encode('UTF-8')
            self.__socket.send(request)
            payload = self.__socket.recv(1024)
            payload = payload.decode('UTF-8')
            recv_data = json.loads(payload)
        finally:
            self.__socketLock.release()
        return recv_data


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s')

    apiConfig = ApiConfig(username='admin', password='123456', gateway_uuid='f2be7746f20f11ecb49d525400ffc252',
                          host='sys.aiotos.net', port=80, token=None)
    httpCallBack = HttpCallBackI()

    while True:
        try:
            d = DriverHttpApi(apiConfig=apiConfig, callBackI=httpCallBack)
            time.sleep(86400)
        except Exception as ex:
            continue
        finally:
            pass
