#!coding:utf8
import json
import sys
import time
sys.path.append("..")
sys.setdefaultencoding( "utf-8" )
from driver import *
import logging

logger = logging.getLogger()
logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s')
logger.setLevel(logging.DEBUG & logging.INFO)

import requests
import json
import datetime
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings()

class XRAPI(object):
    # def __init__(self,host):
    #     self.host = host
    # 获取token
    def GetToken(self):
        url = 'https://app.dtuip.com/oauth/token'
        print url
        params = {
            "grant_type": "password",
            "username": "18358761272",
            "password": "9519513737"
        }
        headers = {
            "authorization": "Basic NWM2NDI0ZjM0ODNkNDFkZWI4MGIyZThkNTJlZDBkYTA6OTNiNTgzNGZlMmJkNDdlNDk5YTQ2YTk3MjA0OGI1Y2Q=",

        }
        res = requests.post(url=url,headers=headers,data=params)
        return json.loads(res.text)

    # 获取设备信息
    def GetMessage(self,accessToken, clientId):
        url ="https://app.dtuip.com/api/device/getDeviceGroup"
        headers = {
            "authorization":"Bearer "+accessToken,
            "tlinkAppId": clientId
        }
        body = {
            "userId":"62382"
        }
        res = requests.get(url=url,headers=headers,data=json.dumps(body))
        return json.loads(res.text)

        # 分页获取设备 获取大炉 小炉温度
    def GetMessage_page(self,accessToken, clientId):
        url ="https://app.dtuip.com/api/device/getDevices"
        headers = {
            "authorization":"Bearer "+accessToken,
            "tlinkAppId": clientId
        }
        body = {
            "userId":"62382",
            "currPage":1
        }
        res = requests.get(url=url,headers=headers,data=json.dumps(body))
        return json.loads(res.text)

    # .分页获取用户传感器
    def GetMessage_page1(self,accessToken, clientId):
        url ="https://app.dtuip.com/api/device/getDeviceSensorDatas"
        headers = {
            "authorization":"Bearer "+accessToken,
            "tlinkAppId": clientId
        }
        body = {
            "userId":"62382",
            "currPage":1
        }
        res = requests.get(url=url,headers=headers,data=json.dumps(body))
        return json.loads(res.text)
    # .根据id查找设备信息
    def GetMessage_id(self,accessToken, clientId,deviceNo,id):
        url ="https://app.dtuip.com/api/device/getSingleDeviceDatas"
        headers = {
            "authorization":"Bearer "+accessToken,
            "tlinkAppId": clientId
        }
        body = {
            "userId":"62382",
            "currPage":1,
            "deviceId":id,
            "deviceNo": deviceNo,
        }
        res = requests.get(url=url,headers=headers,data=json.dumps(body))
        return json.loads(res.text)
    #获取设备数据
    def GetMessage_msg(self,accessToken, clientId,value):
        url ="https://app.dtuip.com/api/device/getSingleSensorDatas"
        headers = {
            "authorization":"Bearer "+accessToken,
            "tlinkAppId": clientId
        }
        body = {
            "userId":"62382",
            "sensorId":value
        }
        res = requests.get(url=url,headers=headers,data=json.dumps(body))
        return json.loads(res.text)

    # 获取设备历史数据
    # 2010263 小炉温度1 2010676 大 炉子温度1
    def GetHistory_msg(self,accessToken,clientId,stTime,sensorId):
        url = "https://app.dtuip.com/api/device/getSensorHistroy"
        d = datetime.datetime.now()
        startDate = str(d.year)+'-'+str(d.month)+'-'+str(d.day)+' '+str(stTime)+':'+'0'+':'+'0'
        endDate = str(d.year)+'-'+str(d.month)+'-'+str(d.day)+' '+str(stTime+1)+':'+'0'+':'+'0'
        # print startDate
        # print endDate
        headers = {
            "authorization":"Bearer "+accessToken,
            "tlinkAppId": clientId
        }
        body = {
            "userId":"62382",
            "sensorId":sensorId,
            "startDate":startDate,
            "endDate":endDate,
            "pagingState":"",
            "pageSize":1
        }
        res = requests.get(url=url, headers=headers, data=json.dumps(body))
        return res.status_code,json.loads(res.text)

class Project(IOTOSDriverI):
    # 1、通信初始化
    def InitComm(self, attrs):
        self.setPauseCollect(False)
        self.setCollectingOneCircle(False)
        self.online(True)


    # #2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
    def Collecting(self, dataId):
    	# '''*************************************************
    	# TODO
    	# **************************************************'''

        # Message = self.obj.GetMessage_msg(self.token['access_token'],self.token['clientId'],2010676)
        # # print Message['value']
        # self.setValue(u'实时温度1', Message['value'])
        self.obj = XRAPI()
        self.token = self.obj.GetToken()
        a = []
        b = []
        for i in range(24):
            res, Message = self.obj.GetHistory_msg(self.token['access_token'], self.token['clientId'], i,2010676)
            if (res == 200):
                if (Message.has_key('dataList')):
                    print '含有dataLIst'
                    a.append(Message['dataList'][0]['val'])
                    # print Message['dataList'][0]['val']
                else:
                    a.append(0)
                    
        for j in range(24):
            res, Message = self.obj.GetHistory_msg(self.token['access_token'], self.token['clientId'], j,2010263)
            if (res == 200):
                if (Message.has_key('dataList')):
                    print '含有dataLIst'
                    b.append(Message['dataList'][0]['val'])
                    # print Message['dataList'][0]['val']
                else:
                    b.append(0)
                    
        #print a
                    
        self.setValue('大炉温度历史数据1',a,auto_created=True)
        self.setValue('小炉温度历史数据1',b,auto_created=True)
        return ()


    # 3、控制
    # 广播事件回调，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 4、查询
    # 查询事件回调，数据点查询访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 5、控制事件回调，数据点控制访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 6、本地事件回调，数据点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})