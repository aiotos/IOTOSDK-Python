#!coding:utf8
import json
import sys
sys.path.append("..")
from driver import *
import random

class Test(IOTOSDriverI):

	#3.1 通信初始化
	def InitComm(self,attrs):
		self.online(True)
		# self.setValue('电压', random.uniform(100,500))
		self.setCollectingOneCircle(False)
		self.setPauseCollect(False)

	#3.3 循环采集
	def Collecting(self, dataId):
		if dataId != 'bc66':
			return ()
		else:
			return [random.uniform(100,500)]

	#3.4 平台下发广播
	def Event_customBroadcast(self, fromUuid, type, data):
		'''
		高级用途，广播事件回调，其他操作访问，详略
		'''
		'''

		TODO 
		
		'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	#3.5 平台下发查询
	def Event_getData(self, dataId, condition):
		'''
		查询事件回调，数据点查询访问
		Parameter
			dataId: string 当前数据点全局标识id，示例如"0b2e"
			condition: string 结合驱动支持下发的自定义查询条件
		Return
		  	string: 查询结果转成string类型返回
		'''
		'''

		TODO 
		
		'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	#3.6 平台下发控制
	def Event_setData(self, dataId, value):
		'''
		控制事件回调，数据点控制访问
		Parameter
			dataId: string 当前数据点全局标识id，示例如"0b2e"
			value: 下发设备控制或写入的值，由数据点自身属性来决定类型，通常为Bool或String
		Return
		  	string: 执行结果转成string类型返回
		'''
		'''

		TODO 
		
		'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	#3.7 订阅数据上报
	def Event_syncPubMsg(self, point, value):
		'''

		TODO 

		'''
		return json.dumps({'code':0, 'msg':'', 'data':''})