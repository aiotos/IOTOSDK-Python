# coding=utf-8
"""三菱PLC-Q13UDVCPU"""
import random
import sys
import json
import time

sys.path.append("..")
from driver import *
from HslCommunication import MelsecMcNet, OperateResult
from library.log_utils import new_logger, logging

logger = new_logger(name=__name__)
logger.setLevel(logging.INFO)


class PLCDataParam(object):
    address = None  # type: str
    length = None  # type: int

    def __init__(self, address=None, length=None, **kwargs):
        self.address = address
        self.length = length

    def __str__(self):
        return json.dumps(self.__dict__)


class PLCData(object):
    name = None # type: str
    desc = None # type: str
    valuetype = None # type: str

    def __init__(self, name=None, valuetype=None, desc=None, **kwargs):
        self.name = name
        self.desc = desc
        self.valuetype = valuetype


class Q13UDVCPUDriver(IOTOSDriverI):

    # 1、通信初始化
    def InitComm(self, attrs):
        self.pauseCollect = False

        threading.Thread(target=self.fetch_loop, args=(attrs, )).start()

    def fetch_loop(self, attrs):
        while True:
            self.fetch_value(attrs=attrs)

    def fetch_value(self, attrs):
        plc_host = self.sysAttrs['config']['param']['host']
        plc_port = self.sysAttrs['config']['param']['port']
        logger.info("plc.host=%s:%s", plc_host, plc_port)
        melsecNet = MelsecMcNet(plc_host, plc_port)  # type: MelsecMcNet
        if melsecNet.ConnectServer().IsSuccess == False:
            raise RuntimeError("connect falied  ")
        else:
            self.online(True)
            # read block
            logger.info("connect succeed!")
            melsecNet.ConnectClose()

        for data_oid, attr in attrs['data'].items():
            try:
                data = PLCData(**attr)
                data_param = PLCDataParam(**attr['config']['param'])
                if data_param.address and data_param.length:
                    new_value = None
                    try:
                        if data.valuetype == 'INT':
                            operateResult = melsecNet.ReadUInt16(data_param.address, data_param.length)  # OperateResult
                        elif data.valuetype == 'BOOL':
                            operateResult = melsecNet.ReadBool(data_param.address, data_param.length)  # OperateResult
                        elif data.valuetype == 'STRING':
                            operateResult = melsecNet.ReadString(data_param.address, data_param.length)  # OperateResult
                        else:
                            logger.error(u"类型不支持:name=%s, type=%s, plc=%s", data.name, data.valuetype, data_param)
                            continue
                        logger.info('%s, %s, %s, %s', data.name, data.valuetype, data_param, operateResult.Content)
                        if operateResult.IsSuccess is False:
                            logger.error(u"读取PCL异常:name=%s, type=%s, plc=%s, error=%s", data.name, data.valuetype,
                                         data_param, operateResult.ToMessageShowString())
                            continue
                        else:
                            if data.valuetype == 'INT':
                                new_value = operateResult.Content[0]
                            elif data.valuetype == 'BOOL':
                                new_value = operateResult.Content[0]
                            elif data.valuetype == 'STRING':
                                new_value = operateResult.Content
                            else:
                                logger.error(u"类型不支持:name=%s, type=%s, plc=%s", data.name, data.valuetype, data_param)
                                continue
                    except Exception as pex:
                        logger.error('%s, %s, %s, %s', data.name, data.valuetype, data_param, pex.__str__(), exc_info=True)
                        continue

                    try:
                        self.setValue(name=data.name, value=new_value)
                    except Exception as eee:
                        logger.error('%s, %s, %s, %s', data.name, data.valuetype, data_param, eee.__str__(), exc_info=True)
                        continue
                else:
                    pass
            except KeyError:
                pass

        try:
            melsecNet.ConnectClose()
        except Exception as ee:
            logger.error("", exc_info=True)


    # 2、采集引擎回调
    def Collecting(self, dataId):
        time.sleep(86400)
        return ()
        '''*************************************************
        TODO
        **************************************************'''
        try:
            # 通过数据点ID获取数据点属性字典
            data_attr = self.data2attrs[dataId]
            value_type = data_attr["valuetype"]
            try:
                if 'minvalue' in data_attr and data_attr['minvalue']:
                    min_value = float(data_attr['minvalue'])
            except:
                min_value = -999
                logger.error('minvalue', exc_info=True)

            try:
                if 'maxvalue' in data_attr and data_attr['maxvalue']:
                    max_value = float(data_attr['maxvalue'])
            except:
                logger.error('maxvalue', exc_info=True)
                max_value = 999

            if value_type == u'BOOL':
                new_value = bool(random.randint(0, 1))
            elif value_type == u'INT':
                new_value = int(random.uniform(min_value, max_value))
            elif value_type == u'FLOAT':
                new_value = float(random.uniform(min_value, max_value))
            else:
                new_value = time.time()
            # logger.info('data=%s, value=%s', dataId, new_value)
            return (new_value,)
        except DataNotExistError as e:
            logger.error(e)
            return None
        time.sleep(0xffff)
        '''*************************************************
        TODO
        **************************************************'''
        return ()

    # 3、控制
    # 广播事件回调，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 4、查询
    # 查询事件回调，数据点查询访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 5、控制事件回调，数据点控制访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 6、本地事件回调，数据点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
