#coding=utf-8
import sys
sys.path.append("..")
import BAC0
import time
from driver import *

class Bacnet(IOTOSDriverI):
    def InitComm(self,attrs):
        #建立连接并且在通路里搜索bacnet设备的ip和设备id
        try:
            self.scan = False
            cfgtmp = attrs['config']['param']
            if 'scan' in cfgtmp:
                self.scan = cfgtmp['scan']
            if not self.scan:
                return
            
            ip = None
            if 'ip' in cfgtmp:
                ip = cfgtmp['ip'] + '/24'
            self.bacnet=BAC0.connect(ip)
            self.bacnet.whois()
            self.online(True)
            self.deviceAddr = None

            # 搜索局域网内的bacnet协议设备并且打印出来，tips 230623，注意，如果此时bacnet采集客户端工具yabe启动链接着在，这里就链接会失败提示端口被占用！反之也一样
            for each in self.bacnet.discoveredDevices:
                deviceName = (self.bacnet.read('%s device %s objectName' % (each[0], each[1])))
                self.deviceAddr = each[0]
                self.error('Found device : %s at address %s' % (deviceName, self.deviceAddr))
                self.error('\r\n')
                # # 打印设备地址为deviceAddr 的objectList property 前十个
                #230623，bacnet.read格式，字符串格式规范，查找设备列表，第几个设备，默认获取第1个发现的设备，注意常量字符串：objectList
                read_pro = self.deviceAddr + ' device %s objectList' %(each[1]) #230623，这里是device 1，对应第一个设备，如果网络多个设备，这里需要对应不同的设备编号
                for item in self.bacnet.read(read_pro):
                    try:
                        devName = item[0]
                        devAddr = str(item[1])
                        if devName == 'device':
                            continue;
                        #230623，bacnet.readMultiple格式，查找点位属性值，注意常量字符串：objectName description presentValue units
                        data_val = self.deviceAddr + ' ' + devName +' '+ devAddr +' '+'objectName description presentValue' #tips 230623 注意，这里对应返回，依次是名称/描述/当前/单位
                        self.debug(data_val)
                        #读取bacnet设备中属性的值
                        data = self.bacnet.readMultiple(data_val)
                        #典型返回内容：['UAI_3', 'Analog Input', 0.0, 'degreesCelsius']
                        # self.setValue(item[0] + '#' + str(item[1]) + '#' + data[0],data[2],auto_created = True)
                        self.setValue('raw',item[0] + '#' + str(item[1]) + '#' + data[0] + '#' + self.deviceAddr + ':' + str(data[2]))
                    except Exception as e:
                        self.warn(e)
                        self.online(False)

        except Exception as e:
            self.error('error!!!!!')
            self.error(e)
            self.online(False)
            # self.bacnet.disconnect()
        
        self.setPauseCollect(True)
        self.setCollectingOneCircle=True
        
        
    def Collecting(self,dataId):
        nametmp = self.name(dataId)
        devName = None
        devNum = None
        if hasattr(self, 'deviceAddr') and self.deviceAddr:
            try:
                if len(nametmp.split('#')) < 2:
                    return ()
                devName = nametmp.split('#')[0]
                devNum = nametmp.split('#')[1]
                data_val = self.deviceAddr + ' ' + devName +' '+ devNum +' '+'objectName description presentValue' #tips 230623 注意，这里对应返回，依次是名称/描述/当前/单位
                self.error(data_val)
                #读取bacnet设备中属性的值
                data = self.bacnet.readMultiple(data_val)
                self.error(data)
                return [data[2]]
            except Exception as e:
                self.error(devName)
                self.error(devNum)
                self.error(e)
        return ()

    def Event_setData(self, dataId, value):
        if 'ip' in self.sysAttrs['config']['param'] and value:
            ip = self.sysAttrs['config']['param']['ip']
        if 'scan' in self.sysAttrs['config']['param']:
            scan = self.sysAttrs['config']['param']['scan']
        if 'sub' in self.sysAttrs['config']['param']:
            sub = self.sysAttrs['config']['param']['sub']
        if sub and len(sub) >= 1:
            devtmp = sub[0].split('.')[0]
            return self.setValue(devtmp + '.' + dataId,value);
        else:
            infotmp = value.split(":")[0]
            valtmp = value.split(":")[1]
            nametmp = '#'.join(infotmp.split('#')[:3])
            matchedIp = infotmp.split('#')[3]
            if matchedIp == ip:
                self.setValue(nametmp, valtmp,auto_created = True)
            else:
                self.error(json.dumps(self.sysAttrs['config']['param']))
                
            #更改bacnet里面属性的值，一般只能是analoValue属性
            nametmp = self.name(dataId)
            try:
                # param = devName.split('#')[0]
                devName = nametmp.split('#')[0]
                devNum = nametmp.split('#')[1]
                data_wri=self.deviceAddr+' '+devName+' '+ devNum +' presentValue ' + str(value)
                self.error(data_wri)
                self.bacnet.write(data_wri)
            except Exception as e:
                self.error(e)
                pass
            return json.dumps({'code': 0, 'msg': '', 'data': ''})
    
    
    #3.7 订阅数据上报
    def Event_syncPubMsg(self, point, value):
        '''
		数据订阅的事件回调。通常是同一个网关下不同设备上报的数据（兼容高级用途跨网关远程数据点订阅，略），常见订阅规则如下，
		在当下驱动对应的设备实例驱动根配置中，param.sub以数组形式订阅同一个网关下其他一个或多个设备上报到平台的数据，以
		[设备全局标识].[数据点全局标识]，其中数据点全局标识可以指定也可以用*代替，表明指定设备所有setValue上报的数据都会
		被订阅过来，在这里做数据处理，通过业务逻辑加工（常见是报文协议解析）后再做上报。
		{
			"driver": "python/dr_xxx.xxxDriver",
			"param": {
				"sub": [
						"1bcd6b35.*"
					],
			}
		}
		Parameter
			point: string 当前数据点全ID（网关标识.设备标识.数据点标识），示例如"9003f858c85011ecbb02525400ffc252.e2c4f6fe.0b2e"，id难以辨别可通过self.name()转换
			value: 上报对应的值，由数据点自身属性来决定类型，通常为Bool/String/Int/Float
		Return
		  	string: string类型返回。注意同网关下不同设备的订阅为异步，可以默认返回或不返。兼容高级用途跨网关远程数据点订阅，返回值解释略
		'''
        '''
		TODO 

		'''
        if 'ip' in self.sysAttrs['config']['param'] and value:
            ip = self.sysAttrs['config']['param']['ip']
            infotmp = value.split(":")[0]
            valtmp = value.split(":")[1]
            nametmp = '#'.join(infotmp.split('#')[:3])
            matchedIp = infotmp.split('#')[3]
            if matchedIp == ip:
                self.online(True);
                self.setValue(nametmp, valtmp,auto_created = True)
        else:
            self.error(json.dumps(self.sysAttrs['config']['param']))
        
        return json.dumps({'code':0, 'msg':'', 'data':''})