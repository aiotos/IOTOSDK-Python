#!coding:utf8
from driver import IOTOSDriverI
import json

# 继承官方驱动类（ZMIotDriverI）
from library.exception import DataNotExistError


# 灵奇地图
class MapDriver(IOTOSDriverI):

    # 1、通信初始化
    def InitComm(self, attrs):
        self.online(True)
        self.critical("deviceId=" + self.sysId)
        self.debug("deviceId=" + self.sysId)
        self.pauseCollect = False

        try:

            # 通过数据点名称，设置多个值
            # values = [
            #     dict(id=self.pointId(self.id(u'经纬度')), value='104.05868130629278,30.595838802691347'),
            #     dict(id=self.pointId(self.id(u'缩放级别')), value=5.66),
            # ]
            # retJson = self.setValues(values)
            # self.info(retJson)
            pass
        except DataNotExistError as e:
            self.warn(e)

    # 2、采集
    def Collecting(self, dataId):
        '''*************************************************
        TODO
        **************************************************'''
        if dataId == 'f66e':
            return ('104.05868130629278,30.595838802691347', )
        elif dataId == '64fc':
            return (15.66, )
        return None

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})