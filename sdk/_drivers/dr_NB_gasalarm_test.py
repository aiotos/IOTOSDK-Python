#coding=utf-8
import sys
sys.path.append("..")
from driver import *
import time
import datetime
from urllib import urlencode
import urllib2
import base64
import hmac
import json
from hashlib import sha1
reload(sys)
sys.setdefaultencoding('utf8')

#签名算法
def signature(key, application, timestamp, param, body):
    code = "application:" + application + "\n" + "timestamp:" + timestamp + "\n"
    for v in param:
        code += str(v[0]) + ":" + str(v[1]) + "\n"
    if (body is not None) and (body.strip()) :
        code += body + '\n'
    # print("param=" + str(param))
    # print("body=" + str(body))
    # print("code=" + str(code))
    return base64.b64encode(hash_hmac(key, code, sha1))

#数据加密
def hash_hmac(key, code, sha1):
    hmac_code = hmac.new(key.encode(), code.encode(), sha1)
    print("hmac_code=" + str(hmac_code.hexdigest()))
    return hmac_code.digest()

#时间戳误差调整
def getTimeOffset(url):
    request = urllib2.Request(url)
    start = int(time.time() * 1000)
    response = urllib2.urlopen(request)
    end = int(time.time() * 1000)

    if response is not None:
        return int(int(response.headers['x-ag-timestamp']) - (end + start) / 2);
    else:
        return 0

baseUrl = 'https://ag-api.ctwing.cn'
timeUrl = 'https://ag-api.ctwing.cn/echo'
offset = getTimeOffset(timeUrl)

#发送http请求函数
def sendSDKRequest(path, head, param, body, version, application, MasterKey, key, method=None, isNeedSort=True,isNeedGetTimeOffset=False):
    paramList = []
    for key_value in param:
        paramList.append([key_value, param[key_value]])
    print("paramList=" + str(paramList))
    if (MasterKey is not None) and (MasterKey.strip()):
        paramList.append(['MasterKey', MasterKey])
    if isNeedSort:
        paramList = sorted(paramList)

    headers = {}
    if (MasterKey is not None) and (MasterKey.strip()):
        headers['MasterKey'] = MasterKey
    headers['application'] = application
    headers['Date'] = str(datetime.datetime.now())
    headers['version'] = version

    temp = dict(param.items())
    if (MasterKey is not None) and (MasterKey.strip()):
        temp['MasterKey'] = MasterKey

    url_params = urlencode(temp)

    url = baseUrl + path
    if (url_params is not None) and (url_params.strip()):
        url = url + '?' + url_params
    print("url=" + str(url))
    global offset
    if isNeedGetTimeOffset:
        offset = getTimeOffset(timeUrl)
    timestamp = str(int(time.time() * 1000) + offset)
    headers['timestamp'] = timestamp
    sign = signature(key, application, timestamp, paramList, body)
    headers['signature'] = sign

    headers.update(head)

    print("headers : %s" % (str(headers)))

    if (body is not None) and (body.strip()):
        request = urllib2.Request(url=url, headers=headers, data=body.encode('utf-8'))
    else:
        request = urllib2.Request(url=url, headers=headers)
    if (method is not None):
        request.get_method = lambda: method
    response = urllib2.urlopen(request)
    if ('response' in vars()):
        print("response.code: %d" % (response.code))
        return response
    else:
        return None

#查询单个设备最新状态(目前还未用到)
def QueryDeviceStatus(appKey, appSecret, body):
    path = '/aep_device_status/deviceStatus'
    head = {}
    param = {}
    version = '20181031202028'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, None, key, 'POST')
    if response is not None:
        return response.read()
    return None

#分页查询设备历史数据
def getDeviceStatusHisInPage(appKey, appSecret, body):
    path = '/aep_device_status/getDeviceStatusHisInPage'
    head = {}
    param = {}
    version = '20190928013337'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, None, key, 'POST')
    if response is not None:
        return response.read()
    return None

#查询事件上报
def QueryDeviceEventList(appKey, appSecret, MasterKey, body):
    path = '/aep_device_event/device/events'
    head = {}
    param = {}
    version = '20210327064751'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, MasterKey, key, 'POST')
    if response is not None:
        return response.read()
    return None

#数据下发函数
def CreateCommand(appKey, appSecret, MasterKey, body):
    path = '/aep_device_command/command'
    head = {}
    param = {}
    version = '20190712225145'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, MasterKey, key, 'POST')
    if response is not None:
        return response.read()
    return None

class Project(IOTOSDriverI):
    def InitComm(self,attrs):
        self.setPauseCollect(False)
        self.setCollectingOneCircle(False)
        self.online(True)

        try:
            #获取中台配置的参数（必不可少）
            self.Nbapplication = self.sysAttrs['config']['param']['application']  # APPKEY
            self.Nbkey = self.sysAttrs['config']['param']['key']  # APPScret
            self.NbMasterKey=self.sysAttrs['config']['param']['MasterKey']  #MasterKey
            # self.NbMasterKey = "5fef44837aa54f42a7a49b73a4d95a15"
            self.NbproductId = self.sysAttrs['config']['param']['productId']
            self.NbdeviceId = self.sysAttrs['config']['param']['deviceId']
        except Exception,e:
            self.debug(u'获取参数失败！'+e.message)

    def Collecting(self, dataId):
        # application = "7CL0z7LNs14"  # APPKEY
        # key = "GkfEquj75z"  # APPScret
        # productId = "15046289"
        # deviceId = "e612d1428a6345778c71e587bfaa5d30"
        try:
            cfgtmp = self.data2attrs[dataId]['config']
            # 过滤掉非采集点
            if cfgtmp["param"] == "":
                return ()

            # 过滤采集点
            if 'disabled' in cfgtmp and cfgtmp['disabled'] == True:
                return ()
            else:
                self.debug(self.name(dataId))

            #请求需要用的参数
            timearry = (datetime.datetime.now() + datetime.timedelta(days=-29)).timetuple()  # 当前时间减去29天后转化为timetuple的格式用于转换成timestamp格式
            begin_timestamp = str(int(time.mktime(timearry) * 1000) + offset)
            end_timestamp = str(int(time.time() * 1000) + offset)
            page_size = "100"
            page_timestamp = ""

            # 上传设备上传数据
            if 'private' in cfgtmp['param'] and cfgtmp['param']['private']=='show_data':

                body_HisInPage = '{"productId":"' + self.NbproductId + '","deviceId":"' + self.NbdeviceId + '","begin_timestamp":"' + begin_timestamp + '","end_timestamp":"' + end_timestamp + '","page_size":' + page_size + ',"page_timestamp":"' + page_timestamp + '"}'

                #http请求，拿去设备上报的数据
                res = getDeviceStatusHisInPage(self.Nbapplication, self.Nbkey, body_HisInPage)
                data = json.loads(res)['deviceStatusList']
                self.debug(data)
                data_dic = {}
                flat = 0
                for i in data:
                    if 'sample_ad_value' in i and flat != 1:
                        data_dic.update(i)
                        flat += 1
                    elif flat == 1:
                        break
                self.debug(data_dic)

                data_list = []
                for key, value in data_dic.items():
                    # 拿到时间戳数据时转化为时间
                    if key == 'timestamp':
                        value = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(value) / 1000))

                    if key=='device_status':
                        status={
                            0:"正常",
                            1:"燃气泄漏报警",
                            2:"传感器故障",
                            3:"自检",
                            4:"失效"
                        }
                        value=status[value]
                    if type(value) == unicode:
                        # unicode转为str类型
                        value = value.encode('utf-8')
                    data_list.append(value)
                print (data_list)
                return tuple(data_list)

            # 上传事件上报的数据
            if 'private' in cfgtmp['param'] and cfgtmp['param']['private']=='gas_event':
                #查询事件上报的数据

                body_event = '{"productId":"' + self.NbproductId + '","deviceId":"' + self.NbdeviceId + '","startTime":"' + begin_timestamp + '","endTime":"' + end_timestamp + '","pageSize":' + page_size + ',"page_timestamp":"' + page_timestamp + '"}'

                event_res = QueryDeviceEventList(self.Nbapplication, self.Nbkey, self.NbMasterKey, body_event)
                self.debug(event_res)
                event_data = json.loads(event_res)['result']['list']
                # self.debug(event_data)
                event_list = []
                for i in event_data:
                    self.debug(i['eventContent'])
                    if 'gas_sensor_state' in i['eventContent']:
                        for key, value in eval(i['eventContent']).items():
                            if key=="gas_sensor_state":
                                status={
                                    0:"正常",
                                    1:"低浓度报警",
                                    2:"高浓度"
                                }
                                value=status[value]
                                event_list.append(value)
                        event_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(i['createTime']) / 1000))
                        event_list.append(event_time)
                        self.debug(event_list)
                        break
                return tuple(event_list)

            return ()

        except Exception,e:
            self.debug(u'数据上传失败'+e.message)


    def Event_setData(self, dataId, value):
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

