#pragma once

#include "ZMIotDriverI.h"
#include "json/json.h"
#include "json/value.h"
#include "json/writer.h"

class CustomDriver :public ZMIotDriverI
{
public:
	CustomDriver();
	virtual ~CustomDriver();

	//一、重写事件函数
	// #1、通信初始化
	virtual void InitComm(const string &params);
	// #2、采集
	virtual string Collecting(const string &dataId);
	// #3、控制
	// # A、广播事件
	virtual string Event_customBroadcast(const string &fromUuid, const string &type, const string &data);
	// # B、事件回调，数据点读操作
	virtual string Event_getData(const string &dataId, const string &condition);
	// # C、事件回调，数据点写操作
	virtual string Event_setData(const string &dataId, const string &value);
	// # D、事件回调，本地订阅数据的发布事件。账户下订阅的所有监测点进行pub时，会到所有接入点的所有设备驱动中走一遍，以数据点全id传入。
	virtual string Event_syncPubMsg(const string &point, const string &value);

protected:

public:
	string m_sysId;
	Json::Value m_sysAttrs;
	Json::Value m_data2attrs;
	Json::Value m_data2subs;
};
