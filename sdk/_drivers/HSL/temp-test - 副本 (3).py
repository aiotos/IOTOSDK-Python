﻿from HslCommunication import SiemensS7Net
from HslCommunication import SiemensPLCS
from HslCommunication import SoftBasic


def printReadResult(result, addr):
    if result.IsSuccess:
        print("success[" + addr + "]   " + str(result.Content))
    else:
        print("failed[" + addr + "]   "+result.Message)


def printWriteResult(result, addr):
    if result.IsSuccess:
        print("success[" + addr + "]")
    else:
        print("falied[" + addr + "]  " + result.Message)


if __name__ == "__main__":
    siemens = SiemensS7Net(SiemensPLCS.S300, "192.168.0.173")
    if siemens.ConnectServer().IsSuccess == False:
        print("connect falied")
    else:
        # read block
        print("connect succeed!")
        lentmp = 200
        read = siemens.Read('M0', lentmp)
        if read.IsSuccess:
            for i in range(0, lentmp):
                numtmp = read.Content[i]
                if numtmp:
                    print 'M%d' % i, bin(numtmp)
        else:
            print(read.Message)
        siemens.ConnectClose()
