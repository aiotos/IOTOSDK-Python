#!coding:utf8
import json
import sys
sys.path.append("..")
from driver import *
from drl_tcp import *
# from .bx_device import DeviceServer, SL651CallbackI
from .SL651_device import SL651DeviceServer as DeviceServer,SL651CallbackI
from .SL651_2014 import SL651Protocol, SL651Response
from .service import Service

class SL651GateDriver(TCPDriver,SL651CallbackI):
	__server = None  # type: DeviceServer
	__service = None  # type: Service

	def InitComm(self,attrs):
		super(SL651GateDriver, self).InitComm(attrs)

	#2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
	def Collecting(self, dataId):
		'''*************************************************
		TODO
		**************************************************'''
		return ()

	#3、控制
	#广播事件回调，其他操作访问
	def Event_customBroadcast(self, fromUuid, type, data):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	# 4、查询
	# 查询事件回调，数据点查询访问
	def Event_getData(self, dataId, condition):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	# 5、控制事件回调，数据点控制访问
	def Event_setData(self, dataId, value):
		super(SL651GateDriver, self).Event_setData(dataId,value)
		return json.dumps({'code':0, 'msg':'', 'data':''})

	# 6、本地事件回调，数据点操作访问
	def Event_syncPubMsg(self, point, value):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	#tcp数据回调
	def tcpCallback(self, data):
		# 默认是原始数据直接上报和本地发布
		# super(SL651GateDriver, self).tcpCallback(data)
		datastr = self.str2hex(data)
		self.info("Master < < < < < < Device: " + datastr)
		#重写，将原始数据经过SL651解析后再报上去同时自动推送给本地订阅的多个设备实例，避免原始数据到每个设备实例驱动中都要去同一个协议解析！
		dataDecoded = SL651Protocol().decode2(data)
		self.setValue('rawData',dataDecoded)
		self.warn('--------responsing-------')
		super(SL651GateDriver, self).send(SL651Protocol().eecode(dataDecoded))
		self.warn('--------responsed!-------')

	# 连接状态回调
	def connectEvent(self, state):
		super(SL651GateDriver, self).connectEvent(state)

