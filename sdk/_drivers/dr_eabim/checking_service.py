# coding=utf-8
"""保存数据到strapi"""
import time
import requests


class CheckingService(object):
    __baseUrl = None  # type: str
    __jwt = None  # type: str
    __session = None  # type: requests.Session

    def __init__(self, baseUrl, jwt):
        self.__baseUrl = baseUrl
        self.__jwt = jwt
        self.__session = requests.Session()
        self.__session.headers['Authorization'] = 'Bearer ' + self.__jwt
        self.__session.headers['Content-Type'] = 'application/json'

    def save_person(self, data):
        """保存人员信息

        :type data: dict
        :return:
        """
        url = '%s/%s' % (self.__baseUrl, 'eabim-people')
        resp = self.__session.get(url=url, params=dict(PersonID=data['PersonID']))
        old_list = resp.json()
        if len(old_list) == 0:
            resp = self.__session.post(url=url, json=data)
            return resp.status_code, resp.json()
        else:
            old = old_list[0]
            url = '%s/%s/%s' % (self.__baseUrl, 'eabim-people', old['id'])
            resp = self.__session.put(url=url, json=data)
            return resp.status_code, resp.json()

    def save_record(self, data):
        """保存考勤记录

        :type data: dict
        :return:
        """
        url = '%s/%s' % (self.__baseUrl, 'eabim-checkings')
        unionId = '%s-%s' % (data['host'], data['RecordID'])
        data['unionId'] = unionId
        resp = self.__session.get(url=url, params=dict(unionId=unionId))
        old_list = resp.json()
        if len(old_list) == 0:
            resp = self.__session.post(url=url, json=data)
            return resp.status_code, resp.json()
        else:
            old = old_list[0]
            url = '%s/%s/%s' % (self.__baseUrl, 'eabim-checkings', old['id'])
            resp = self.__session.put(url=url, json=data)
            return resp.status_code, resp.json()

    def get_lastRecordTime(self, host):
        url = '%s/%s' % (self.__baseUrl, 'eabim-checkings')
        resp = self.__session.get(url=url, params=dict(host=host, _sort='RecordTime:desc', _limit=1))
        datas = resp.json()
        if datas and len(datas) > 0:
            return int(float(datas[0]['RecordTime']))
        return 0
