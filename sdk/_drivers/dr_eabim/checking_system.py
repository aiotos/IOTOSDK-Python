# coding=utf-8
"""考勤人员"""

import threading
import requests
from datetime import datetime
from driver import IOTOSDriverI
import json, time, random
from library.exception import DataNotExistError
import logging
import uuid
import paho.mqtt.client as mqtt
from .dto import DeviceConfig, CkPerson, CkRecord

logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s %(name)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
from checking_test import CheckingApi
from checking_service import CheckingService

"""
{
  "parentid": null,
  "driver": "python/dr_eabim.CkSystemDriver",
  "param": {
    "deviceType": "D",
    "deviceId": "XW202100000221",
    "username": "admin", "password": "123456",
    "baseUrl": 'http://localhost:31337', 'jwt': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM3NDk2MDcyLCJleHAiOjE2NDAwODgwNzJ9.h5zx81JsSIJZB2d877z2KruPPBzyceBzxLwSDgEthuM",
    "hosts": ["192.168.3.241", "192.168.3.242", "192.168.3.243", "192.168.3.244", "192.168.3.245", "192.168.3.246", "192.168.3.247", "192.168.3.248", "192.168.3.249", "192.168.3.250", "192.168.3.251", "192.168.3.252"],
    "sub": [
    ]
  },
  "bind": {
  }
}
"""


class CkSystemDriver(IOTOSDriverI):
    __ckApi = None  # type: CheckingApi
    __config = None  # type: DeviceConfig
    __ckService = None  # type:

    # 1、通信初始化
    def InitComm(self, attrs):
        self.online(True)
        # self.setValue(value='auto_created', name='CLI_auto_created')
        self.pauseCollect = False
        self.collectingOneCircle = True
        self.__config = DeviceConfig(**attrs['config']['param'])
        self.__ckService = CheckingService(baseUrl=self.__config.baseUrl, jwt=self.__config.jwt)
        # threading.Thread(target=self.__sync_data, name='sync_data').start()
        threading.Thread(target=self._fetch_persons, name='__fetch_persons').start()
        threading.Thread(target=self._sync_record, name='_sync_record').start()

    def __sync_data(self):
        logger.info(self.__config.__dict__)
        # self.__fetch_persons()
        self.__sync_record()

    def _fetch_persons(self):
        while True:
            for host in self.__config.hosts:
                try:
                    self.__fetch_persons(host=host)
                except Exception as ex:
                    logger.error('_fetch_persons, host=%s', host, exc_info=True)
            time.sleep(60 * 10)

    def __fetch_persons(self, host):
        """同步员工信息"""
        rd = self.setValue('personsTime', time.time())
        logger.info('persons_time=%3.f, r=%s', time.time(), rd)
        ckApi = CheckingApi(host=host, username=self.__config.username, password=self.__config.password)
        rs = ckApi.loadPersonsByDepartID()
        if 'Persons' in rs:
            logger.info('loadPersonsByDepartID, host=%s, PersonsLen=%s, rs=%s', host, len(rs['Persons']), rs)
        else:
            logger.warning('loadPersonsByDepartID, host=%s, rs=%s', host, rs)
            return

        data_list = []
        for r in rs[u'Persons']:
            ckPer = CkPerson(**r)

            rp = ckApi.loadPersonPicture(PersonID=ckPer.PersonID)
            try:
                # print(r['DepartID'], r['PersonID'], rp['Picture'])
                ckPer.Picture = rp['Picture']
            except KeyError as e:
                print(r['DepartID'], r['PersonID'], r['PersonName'], rp['errMsg'])
            except Exception as e:
                print (e)
            try:
                status_code, rs = self.__ckService.save_person(data=ckPer.__dict__)
                if status_code == 200:
                    logger.info("ckService.save_person, rs=%s", status_code)
                else:
                    logger.error("ckService.save_person, rs=%s, err=%s", status_code, rs)
            except Exception as ex:
                logger.error("ckService.save_person, err=%s", ex.__str__(), exc_info=True)
            data_list.append(ckPer.__dict__)
        # r = self.setValue('persons', json.dumps(data_list))
        # logger.info('%s', r)

    def _sync_record(self):
        time.sleep(30)
        while True:
            self.setValue('recordsUpdateTime', time.time())
            for host in self.__config.hosts:
                try:
                    lastRecordTime = self.__ckService.get_lastRecordTime(host=host)
                    logger.info('host=%s, lastRecordTime=%s', host, lastRecordTime)
                    self.__sync_record(host=host, startTime=lastRecordTime)
                except Exception as ex:
                    logger.error("__sync_record", exc_info=True)
            time.sleep(60 * 5)

    def __sync_record(self, host, startTime):
        """同步考虑记录
        @param startTime:
        """
        ckApi = CheckingApi(host=host, username=self.__config.username, password=self.__config.password)
        rs = ckApi.queryRecords(startTime=startTime)
        logger.info('host=%s, startTime=%s, len=%s', host, startTime, len(rs['Records']))
        for r in rs['Records']:
            ckp = CkRecord(host=host, **r)
            logger.info("%s, %s, %s, %s", ckp.RecordID, ckp.PersonID, ckp.PersonName, ckp.RecordTime)
            rp = ckApi.loadRecordPicture(FileName=ckp.RecordPicture)
            try:
                ckp.RecordPicture = rp['Picture']
            except Exception as ex:
                pass
            try:
                status_code, rs = self.__ckService.save_record(data=ckp.__dict__)
                if status_code == 200:
                    logger.info("ckService.save_record, rs=%s", status_code)
                else:
                    logger.error("ckService.save_record, rs=%s, err=%s", status_code, rs)
            except Exception as ex:
                logger.error("ckService.save_record, err=%s", ex.__str__(), exc_info=True)
            # rd = self.setValue('records', json.dumps(ckp.__dict__))
            # logger.info('ck=%s', rd)

    firstId = None
    pollTime = None

    # 2、采集
    def Collecting(self, dataId):
        return ()

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        logger.error('dataId=%s, value=%s', dataId, value)
        return json.dumps({'code': 0, 'msg': '', 'data': dict(response_time=time.time())})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
