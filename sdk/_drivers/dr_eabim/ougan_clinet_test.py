# coding=utf-8
"""欧感第二批设备，客户端模拟测试"""
import socket
import time

datas = [
    # '7f 32 08 02 14 02 81 49 60 98 08 20 00 00 01 00 d2 00 02 06 00 22 03 29 15 27 32 03 0e 00 99 fd c7 00 78 00 68 0e 00 00 45 f6 86 0c 20 b4 00 04 50 06 21 90 42 2e 99 fd 37 bc e1 42 e9 be e1 41 22 03 28 13 02 00 00 00 0b 0b 00 00 00 02 02 3d aa e6 43 00 00 8c 41 13 00 00 00 02 02 00 00 82 43 00 80 86 41 0e 00 00 00 02 02 eb 31 db 43 00 80 8d 41 0f 00 00 00 02 02 eb f1 da 43 00 00 90 41 16 00 00 00 02 02 70 fd 6b 43 00 80 8a 41 19 00 00 00 02 02 52 58 cf 43 00 80 95 41 15 00 00 00 02 02 ae c7 df 43 00 80 92 41 14 00 00 00 02 02 29 9c 72 43 00 80 8d 41 17 00 00 00 02 02 85 4b d9 43 00 00 93 41 0d 00 00 00 02 02 cd ec 14 44 00 80 86 41 02 00 00 00 09 02 33 33 80 42 cc cc a0 41 01 ff 62 16',
    # '7f 32 08 02 14 02 81 49 60 98 08 20 00 00 01 00 ee 00 02 06 00 22 03 29 17 25 28 03 0e 00 bd fd ec 00 ad 00 66 0e 00 00 47 7f 84 0c 20 ca 00 04 82 06 21 90 a7 2e bd fd fc 0a f1 42 e2 db f0 41 22 03 29 17 25 00 00 00 08 0c 00 00 00 02 02 47 51 1e 44 00 00 75 41 10 00 00 00 02 02 47 f1 07 44 00 00 7d 41 11 00 00 00 02 02 d7 23 07 44 00 00 73 41 12 00 00 00 02 02 c2 d5 ac 43 00 00 75 41 e3 ef 03 00 0c 02 ae 47 0c 42 9a 99 81 41 e5 ef 03 00 0c 02 48 e1 ec 41 00 00 80 41 5a 00 00 00 08 0a 90 01 c8 3a e0 01 f0 3a e0 01 70 3a 2e 4a 22 3a 4a ec bf 39 f5 2c 1f 3d a4 91 4f 3f 4b f2 00 3f 46 17 b2 c2 93 d5 9d 41 50 00 00 00 08 0a 72 01 39 3c 68 01 b4 3b 04 01 82 3b ea cd db 3a ff b1 74 3b 2d 6b 21 3d 81 1a 1b 40 8a c5 b2 c0 8b d0 a7 c2 30 9a 57 41 07 07 07 07 07 07 07 80 b1 16'
'\x7f2\x08\x18\x18\x02\x91I`\x98\x08 \x00\x00\x01\x00~\x00\x02\x06\x00"\x06\x01\x19\x02%\x03\x0e\x00?\xfcf\x00\x8c\x00f\x0e\x00\x00\x83\xf5K\t R\x00\x04@\x00"\x90F\x0e?\xfc\x00\x00\x00\x00\x00\x00\x00\x00"\x06\x01\x19\x01\x00\x00\x00\x04*\x00\x00\x00\x02\x02\x00\x00\x00\x00\x00\x00\x00\x00%\x00\x00\x00\x02\x02\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\t\x02\x00\x00\x00\x00\x00\x00\x00\x00\t\x00\x00\x00\x04\x02\x00\x00\x00\x00\x00\x00\x00\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x8cs\x16'
]

if __name__ == '__main__':
    host = '139.196.104.76'
    port = 2022
    addr = (host, port)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 连接server
    client.connect(addr)
    # 向server发送数据

    for data in datas: # type: str
        data = data.upper()
        print(data)
        try:
            rs = client.send(data.encode('utf-8'))
        except Exception as ex:
            rs = client.send(data)
        print(rs)
        time.sleep(1)
        # 接收server返回的数据
        revcdata = client.recv(1024)
        # 收到的数据都是bytes类型
        print(revcdata.decode(encoding='utf-8'))
        time.sleep(1)

    client.close()