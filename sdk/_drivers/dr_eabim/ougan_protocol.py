# coding=utf-8
"""欧感协议解析"""
import logging
import time

log_format = '%(asctime)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=log_format)
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)


def ougan2_decode(data_text):
    """第二批设备解析"""
    message_data = dict()
    data_hexs = []
    index = 0
    while index < len(data_text):
        data_hexs.append(data_text[index:index + 2])
        index += 2
    if data_hexs[0] != '7F':
        raise ValueError('报文格式不合法')
    data_hexs.pop(0)
    device_type = data_hexs.pop(0)
    message_data['device_type'] = device_type
    id_len = int(data_hexs.pop(0))
    logger.debug('id_len:%s', id_len)
    device_id = data_hexs[0:id_len]
    data_hexs = data_hexs[id_len:]
    device_id.reverse()
    device_id = ''.join(device_id)  # type: str
    logger.debug('device_id:%s', device_id)
    # 协议号码 + 密钥
    pwd = data_hexs.pop(0)
    logger.debug('协议号码 + 密钥=>%s', pwd)
    # 帧顺序号
    index = data_hexs[0:2]
    logger.debug('帧顺序号:%s', ' '.join(index))
    data_hexs = data_hexs[2:]
    # 信息域长度和类型
    index = data_hexs[0:2]
    logger.debug('信息域长度和类型:%s', ' '.join(index))
    data_hexs = data_hexs[2:]

    # 长度74字节
    index = data_hexs[0:2]
    logger.debug('长度:%s', ' '.join(index))
    data_hexs = data_hexs[2:]

    # 本地时间上报命令字
    index = data_hexs[0:1]
    logger.debug('本地时间上报命令字:%s', ' '.join(index))
    data_hexs = data_hexs[1:]

    # 字节长度14
    step = 2
    index = data_hexs[0:step]
    logger.debug('字节长度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 时间
    step = 6
    index = data_hexs[0:step]
    logger.debug('时间:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 网络状态命令字
    step = 1
    index = data_hexs[0:step]
    logger.debug('网络状态命令字:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 字节长度
    step = 2
    index = data_hexs[0:step]
    logger.debug('字节长度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 信号强度
    step = 2
    index = data_hexs[0:step]
    logger.debug('信号强度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 信号比
    step = 2
    index = data_hexs[0:step]
    logger.debug('信号比:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 小区物理标志
    step = 2
    index = data_hexs[0:step]
    logger.debug('小区物理标志:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 无线频点，
    step = 4
    index = data_hexs[0:step]
    logger.debug('无线频点:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 小区识别号
    step = 4
    index = data_hexs[0:step]
    logger.debug('小区识别号:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 当前数据上报命令字
    step = 1
    index = data_hexs[0:step]
    logger.debug('当前数据上报命令字:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 数据字节数据
    step = 2
    index = data_hexs[0:step]
    logger.debug('数据字节数据:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 编号长度
    step = 1
    index = data_hexs[0:step]
    logger.debug('编号长度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 设备地址
    step = 4
    raw_index = index = data_hexs[0:step]
    raw_index.reverse()
    device_id = ''.join(raw_index)
    logger.debug('设备地址:%s=>%s', ' '.join(index), ''.join(raw_index))
    data_hexs = data_hexs[step:]

    # 设备电压
    step = 2
    index = data_hexs[0:step]
    logger.debug('设备电压:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 信号强度
    step = 2
    index = data_hexs[0:step]
    logger.debug('信号强度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 经度
    step = 4
    index = data_hexs[0:step]
    logger.debug('经度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 纬度
    step = 4
    index = data_hexs[0:step]
    logger.debug('纬度:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 时间
    step = 6
    index = data_hexs[0:step]
    logger.debug('时间:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 振弦数据数量
    step = 1
    index = data_hexs[0:step]
    logger.debug('振弦数据数量:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 模拟量数量
    step = 1
    index = data_hexs[0:step]
    logger.debug('模拟量数量:%s', ' '.join(index))
    data_hexs = data_hexs[step:]

    # 数字传感器数量
    step = 1
    index = data_hexs[0:step]
    logger.debug('数字传感器数量:%s->%s', ' '.join(index), int(index[0], 16))
    data_hexs = data_hexs[step:]

    tmp_hexs = data_hexs[-4:]
    data_hexs = data_hexs[0:-4]
    logger.debug('tmp_hexs:%s', ' '.join(tmp_hexs))

    if tmp_hexs[3] == '16':
        pass
    else:
        raise ValueError('报文不合法')

    data_list = [] # type: list[dict]
    _dataIndex = 0
    while len(data_hexs) > 0:
        _dataIndex += 1
        logger.debug('%s【数据%s开始】%s', ' -' * 15, _dataIndex, ' -' * 15)

        # 数字传感器地址
        step = 4
        index = data_hexs[0:step]
        device_addr = int(index[0], 16)
        logger.debug('数字地址:%s=>%s', ' '.join(index), int(index[0], 16))
        data_hexs = data_hexs[step:]

        # 传感器协议类型
        step = 1
        index = data_hexs[0:step]
        device_type = int(index[0], 16)
        logger.debug('传感器协议类型:%s=>%s', ' '.join(index), device_type)
        data_hexs = data_hexs[step:]

        # 传感器数据数量
        step = 1
        index = data_hexs[0:step]
        logger.debug('传感器数据数量:%s', ' '.join(index))
        data_hexs = data_hexs[step:]

        # 数据1， 湿度
        step = 4
        index = data_hexs[0:step]
        logger.debug('数据1:%s=>%s', ' '.join(index), int2float(''.join(index)))
        data_value1 = int2float(''.join(index))
        data_hexs = data_hexs[step:]

        # 数据2， 温度
        step = 4
        index = data_hexs[0:step]
        logger.debug('数据2:%s=>%s', ' '.join(index), int2float(''.join(index)))
        data_value2 = int2float(''.join(index))
        data_hexs = data_hexs[step:]
        # logger.debug(data_hexs)

        # # 设备补码
        # step = 4
        # index = data_hexs[0:step]
        # device_addr = int(index[0], 16)
        # logger.debug('数字地址:%s=>%s', ' '.join(index), int(index[0], 16))
        # data_hexs = data_hexs[step:]
        logger.debug('%s【数据%s结束】%s', ' -' * 15, _dataIndex, ' -' * 15)
        iden = '%s>%s>%s' % (device_id, device_type, device_addr, )
        parse_data = '%s>%s,%s' % (iden, data_value1, data_value2, )
        data_list.append(dict(device_id=iden, device_type=device_type, parse_data=parse_data))

    return data_list


# 高位低位位置调整
def place_correct(a):
    new_a = ""
    if len(a) % 2 == 0:
        for i in range(0, len(a) + 1, 2):
            new_a = new_a + a[len(a) - i:len(a) - i + 2]

    else:
        pass

    return new_a


# int转float
import struct


def int2float(a):
    z = a
    # print z
    # print z(123)
    f = struct.unpack('<f', z.decode('hex'))[0]  # 返回浮点数
    # f = struct.unpack('<f', z)[0]  # 返回浮点数
    return f


def xxx(data_text):
    set_data = data_text
    logger.debug(set_data)
    # 设备类型
    dev_type = bin(int(set_data[2:4]))
    dev_type = set_data[2:4]
    logger.debug('设备类型：%s', dev_type)
    # 设备参数长度
    dev_addr_len = int(bin(int(set_data[4:6]))[-4:], 2)

    # 设备编号
    dev_addr_endindex = 6 + dev_addr_len * 2
    dev_addr = set_data[6:dev_addr_endindex]
    # print ("设备地址：{}".format(place_correct(dev_addr)))

    # 控制域
    ctl_area = set_data[dev_addr_endindex:(dev_addr_endindex + 6)]
    # print ("控制域：{}".format(ctl_area))

    # 信息域长度及类型
    info_area = set_data[(dev_addr_endindex + 6):(dev_addr_endindex + 10)]
    # print ("信息域长度及类型：{}".format(info_area))

    # 信息长度
    info_data_len = set_data[(dev_addr_endindex + 12):(dev_addr_endindex + 14)] + set_data[
                                                                                  (dev_addr_endindex + 10):(
                                                                                          dev_addr_endindex + 12)]
    # print ("信息长度：{}".format(info_data_len))

    # 设备传过来的信息
    info_data = set_data[(dev_addr_endindex + 14):(dev_addr_endindex + 14 + int(info_data_len, 16) * 2)]
    # print ("设备信息：{}".format(info_data))

    while len(info_data) != 0:
        # 本地时间上报
        if info_data[0:2] == '02':
            # print ("时间信息长度：{} 和 时间为：{}".format(info_data[2:6], info_data[6:18]))
            info_data = info_data[18:]

        # 网络状态
        elif info_data[0:2] == '03':
            # print ("网络状态信息长度：{} 和 具体信息为：{}".format(info_data[2:6], info_data[6:34]))
            info_data = info_data[34:]

        elif info_data[0:2] == '20':
            # 数据字节数
            data_len = int(info_data[4:6] + info_data[2:4], 16)

            # 设备编号长度
            dev_num_len = info_data[6:8]
            # print ("设备编号长度为：{}".format(dev_num_len))

            dev_num = info_data[8:8 + int(dev_num_len, 16) * 2]
            # print ("设备编号为：{}".format(place_correct(dev_num)))
            # print ("设备其他信息：{}".format(
            #     info_data[(8 + int(dev_num_len, 16) * 2):(8 + int(dev_num_len, 16) * 2 + 18 * 2)]))

            other_info_data_endindex = 8 + int(dev_num_len, 16) * 2 + 18 * 2

            sensor_num = info_data[other_info_data_endindex + 4:other_info_data_endindex + 6]
            # print ("传感器的数量:{}".format(sensor_num))

            sensor_data_addr = info_data[other_info_data_endindex + 6:other_info_data_endindex + 14]
            sensor_data_addr = place_correct(sensor_data_addr)
            # print ("传感器的地址:{}".format(place_correct(sensor_data_addr)))

            sensor_data_type = info_data[other_info_data_endindex + 14:other_info_data_endindex + 16]
            # print ("传感器协议类型:{}".format(sensor_data_type))

            sensor_data_num = info_data[other_info_data_endindex + 16:other_info_data_endindex + 18]
            # print ("传感器数据数量:{}".format(sensor_data_num))

            sensor_num_endindex = other_info_data_endindex + 18

            sensor_datas = []
            sensor_data_endindex = 0
            for i in range(0, int(sensor_data_num, 16)):
                sensor_data = info_data[sensor_num_endindex + i * 8:sensor_num_endindex + i * 8 + 8]
                sensor_datas.append(round(int2float(sensor_data), 1))
                sensor_data_endindex = sensor_num_endindex + i * 8 + 8

            # 设备补码
            dev_complement_ = info_data[sensor_data_endindex:sensor_data_endindex + 10]

            info_data = info_data[sensor_data_endindex + 10:]

            time_stop = time.time()

            set_id = dev_addr + '>' + sensor_data_type + '>' + sensor_data_addr


if __name__ == '__main__':
    data_tmp = '7f 32 08 02 14 02 81 49 60 98 08 20 00 00 01 00 d2 00 02 06 00 22 03 29 15 27 32 03 0e 00 99 fd c7 00 78 00 68 0e 00 00 45 f6 86 0c 20 b4 00 04 50 06 21 90 42 2e 99 fd 37 bc e1 42 e9 be e1 41 22 03 28 13 02 00 00 00 0b 0b 00 00 00 02 02 3d aa e6 43 00 00 8c 41 13 00 00 00 02 02 00 00 82 43 00 80 86 41 0e 00 00 00 02 02 eb 31 db 43 00 80 8d 41 0f 00 00 00 02 02 eb f1 da 43 00 00 90 41 16 00 00 00 02 02 70 fd 6b 43 00 80 8a 41 19 00 00 00 02 02 52 58 cf 43 00 80 95 41 15 00 00 00 02 02 ae c7 df 43 00 80 92 41 14 00 00 00 02 02 29 9c 72 43 00 80 8d 41 17 00 00 00 02 02 85 4b d9 43 00 00 93 41 0d 00 00 00 02 02 cd ec 14 44 00 80 86 41 02 00 00 00 09 02 33 33 80 42 cc cc a0 41 01 ff 62 16'
    data_tmp = '7f 32 08 02 14 02 81 49 60 98 08 20 00 00 01 00 ee 00 02 06 00 22 03 29 17 25 28 03 0e 00 bd fd ec 00 ad 00 66 0e 00 00 47 7f 84 0c 20 ca 00 04 82 06 21 90 a7 2e bd fd fc 0a f1 42 e2 db f0 41 22 03 29 17 25 00 00 00 08 0c 00 00 00 02 02 47 51 1e 44 00 00 75 41 10 00 00 00 02 02 47 f1 07 44 00 00 7d 41 11 00 00 00 02 02 d7 23 07 44 00 00 73 41 12 00 00 00 02 02 c2 d5 ac 43 00 00 75 41 e3 ef 03 00 0c 02 ae 47 0c 42 9a 99 81 41 e5 ef 03 00 0c 02 48 e1 ec 41 00 00 80 41 5a 00 00 00 08 0a 90 01 c8 3a e0 01 f0 3a e0 01 70 3a 2e 4a 22 3a 4a ec bf 39 f5 2c 1f 3d a4 91 4f 3f 4b f2 00 3f 46 17 b2 c2 93 d5 9d 41 50 00 00 00 08 0a 72 01 39 3c 68 01 b4 3b 04 01 82 3b ea cd db 3a ff b1 74 3b 2d 6b 21 3d 81 1a 1b 40 8a c5 b2 c0 8b d0 a7 c2 30 9a 57 41 07 07 07 07 07 07 07 80 b1 16'
    data_tmp = '7F3208181802914960980820000001007E00020600220601190225030E003FFC66008C00660E000083F54B092052000440002290460E3FFC00000000000000002206011901000000042A000000020200000000000000002500000002020000000000000000020000000902000000000000000009000000040200000000000000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F8C7316'
    # data_tmp = '7f 32 08 02 14 02 81 49 60 98 08 20 00 00 01 00 d2 00 02 06 00 22 03 29 15 27 32 03 0e 00 99 fd c7 00 78 00 68 0e 00 00 45 f6 86 0c 20 b4 00 04 50 06 21 90 42 2e 99 fd 37 bc e1 42 e9 be e1 41 22 03 28 13 02 00 00 00 0b 0b 00 00 00 02 02 3d aa e6 43 00 00 8c 41 13 00 00 00 02 02 00 00 82 43 00 80 86 41 0e 00 00 00 02 02 eb 31 db 43 00 80 8d 41 0f 00 00 00 02 02 eb f1 da 43 00 00 90 41 16 00 00 00 02 02 70 fd 6b 43 00 80 8a 41 19 00 00 00 02 02 52 58 cf 43 00 80 95 41 15 00 00 00 02 02 ae c7 df 43 00 80 92 41 14 00 00 00 02 02 29 9c 72 43 00 80 8d 41 17 00 00 00 02 02 85 4b d9 43 00 00 93 41 0d 00 00 00 02 02 cd ec 14 44 00 80 86 41 02 00 00 00 09 02 33 33 80 42 cc cc a0 41 01 ff 62 16'
    data_tmp = data_tmp.strip().replace(' ', '').upper()
    # data_text = '7F320802140281496098082000000100D200020600220329152732030E0099FDC7007800680E000045F6860C20B4000450062190422E99FD37BCE142E9BEE14122032813020000000B0B00000002023DAAE64300008C4113000000020200008243008086410E0000000202EB31DB4300808D410F0000000202EBF1DA430000904116000000020270FD6B4300808A411900000002025258CF4300809541150000000202AEC7DF4300809241140000000202299C724300808D41170000000202854BD943000093410D0000000202CDEC14440080864102000000090233338042CCCCA04101FF6216'
    # print(data_tmp == data_text)
    data = ougan2_decode(data_text=data_tmp)
    print(data)
