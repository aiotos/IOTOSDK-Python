# coding=utf-8
"""数据结构定义"""


class DeviceConfig(object):
    hosts = None # type: list[str]
    username = None # type: str
    password = None # type: str
    baseUrl = None # type: str
    jwt = None # type: str

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


class CkRecord(object):
    """考勤记录"""
    id = None  # type: str # host-RecordID
    host = None  # type: str # 记录ID
    RecordID = None  # type: int # 记录ID
    PersonID = None  # type: int # 人员ID
    PersonIndex = None  # type: int # 人员索引
    PersonName = None  # type: int # 人员姓名
    RecordPass = None  # type: int # 通行类型
    RecordType = None  # type: int # 记录类型
    RecordTime = None  # type: int # 记录时间
    RecordPicture = None  # type: int # 抓拍图片
    ICCardNO = None  # type: int #
    Price = None  # type: int #
    Similarity = None  # type: float # 相似度
    Temperature = None  # type: float # 体温
    GauzeResult = None  # type: float #
    QRCode = None  # type: float #
    FaceRect = None  # type: float #
    IDCardInfo = None  # type: float #
    PriceID = None  # type: float #
    SatetyHatResult = None  # type: float #

    def __init__(self, host, **kwargs):
        self.host = host
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.id = '%s-%s' % (self.host, self.RecordID)


"""
{u'DepartID': 1, u'Password': u'', u'PersonName': u'\u848b\u80e1\u658c', u'FeatureType': 1, u'PersonID': u'1', u'EffectiveTime': 0, 
u'FacePicture': u'/home/data/person//1-face.jpeg', u'WorkID': u'', u'Authority': 1, u'ExpireTime': 4294967295, 
u'ICCardID': u'1', u'ICCardID2': u'', u'PersonPicture': u'/home/data/person//1.jpeg', 
u'Gender': 1, u'PassTime': [{u'EndTime': 1439, u'Enable': 1, u'StartTime': 0}, {u'EndTime': 0, u'Enable': 0, u'StartTime': 0}, {u'EndTime': 0, u'Enable': 0, u'StartTime': 0}, {u'EndTime': 0, u'Enable': 0, u'StartTime': 0}], 
u'FaceID': u'', u'IDCardInfo': None, u'IDCardNo': u'', u'PersonID2': u'', u'QRCode': u''}
"""


class CkPassTime(object):
    Enable = None  # type: int
    EndTime = None  # type: int
    StartTime = None  # type: int


class CkPerson(object):
    """考勤人员信息"""
    DepartID = None  # type: int
    Password = None  # type: str
    PersonName = None  # type: str
    FeatureType = None  # type: int
    PersonID = None  # type: int
    EffectiveTime = None  # type: int
    FacePicture = None  # type: str
    WorkID = None  # type: str
    Authority = None  # type: int
    ExpireTime = None  # type: int
    ICCardID = None  # type: str
    ICCardID2 = None  # type: str
    PersonPicture = None  # type: str
    Gender = None  # type: int
    PassTime = None  # type: [CkPassTime]
    FaceID = None  # type: str
    IDCardInfo = None  # type: dict
    IDCardNo = None  # type: str
    PersonID2 = None  # type: str
    QRCode = None  # type: str
    Picture = None # type: str

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
