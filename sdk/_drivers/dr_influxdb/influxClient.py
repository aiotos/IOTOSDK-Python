# -*- coding: utf-8 -*-
'''
@project: iotos_paas
@file: influx_views.py
@time: 2021/5/17 上午11:25
'''
import datetime
import re
import string
import random
import time
import json
import traceback
from influxdb import InfluxDBClient

class InfluxDB:

    def __init__(self):
        self.influx = InfluxDBClient('203.189.6.3', '8086','iotos', 'Iotos12345678', 'telegraf')

    def stamp_to_utedatetime(self, stamp):
        try:
            lentmp = len(str(stamp))
            hasdot = len(str(stamp).split('.')) >= 2
            if lentmp >= 13:
                if not hasdot or len(str(stamp).split('.')[0]) >= 13: #如果原本就是xxxx.xxx这样13位格式的stamp，就不动。以及包含类似1669021886.0而非1669186961931.0的情况！
                    stamp = float(str(stamp)[:10] + "." + str(stamp)[10:13])
            else:
                # def suff0(num):
                #     suff = ''
                #     for i in range(0,num):
                #         suff = suff + '0'
                #     return suff
                # if not hasdot:
                #     stamp = float(str(stamp)[:10] + "." + str(stamp)[10:] + suff0(13 - lentmp))
                # else:
                #     stamp = float(str(stamp) + suff0(13 + 1 - lentmp))

                if not hasdot:
                    stamp = float(str(stamp)[:10] + "." + str(stamp)[10:])
            return datetime.datetime.utcfromtimestamp(stamp).isoformat("T")
        except Exception as e:
            traceback.print_exc()

    # 获取唯一标识符号
    def get_influx_flag(self, code):
        # 随机6为字母和数字
        num = string.ascii_letters + string.digits
        dom = "".join(random.sample(num, 8))
        return "{}{}{}".format(code, time.strftime('%Y%m%d%H%M%S', time.localtime(time.time())), dom.upper())

    def set_influxdb_data(self, data):
        dataoid = data['data_oid']
        devoid = data['dev_info'].split('——')[1]
        data_name = data['name']
        dev_name = data['dev_info'].split('——')[0]
        time = self.stamp_to_utedatetime(float(data['ts']))

        #兼容上传是s而不是ms时间戳的情况，此时是没有，实际没用，存储数据错误：partial write: points beyond retention policy dropped=1
        #并不是因为时间戳是s不是ms格式导致的，而是因为指定时间进行保存不是自动生成的时间戳，且改时间超过最大保存时间策略了！！
        if len(time.split('.')) < 2:
            time = time +'.000000'

        value = data['val']
        point_list = []
        write_sql = {
            "measurement": "iot_point",
            "tags": {
                "dataoid": dataoid,
                "devoid": devoid,
                "data_name": data_name,
                "dev_name": dev_name
            },
            "time": time,
            "fields": {
                "value": value
            }
        }

        if data['val_type'] == 'INT' or data["val_type"] == 'BOOL':
            write_sql['measurement'] = "iot_point_float"
            write_sql['fields']['value'] = float(value)
            point_list.append(write_sql)

        elif data["val_type"] == 'FLOAT':
            write_sql['measurement'] = "iot_point_float"
            write_sql['fields']['value'] = float(value)
            point_list.append(write_sql)

        # 如果是json类型，将拆分k-v进行存储
        elif data['val_type'] == 'JSON':
            # 组装父数据
            flag = self.get_influx_flag(dataoid)
            write_sql['tags']['is_parent'] = 1
            write_sql['tags']['flag'] = flag
            point_list.append(write_sql)
            # 拆分子元素
            if isinstance(value, str):
                value = eval(value)
            for k, v in value.items():
                info = {
                    "measurement": "iot_point",
                    "tags": {
                        "dataoid": dataoid,
                        "devoid": devoid,
                        "data_name": k,
                        "is_parent": 0,
                        "flag": flag
                    },
                    "time": time,
                    "fields": {
                        "value": v
                    }
                }
                if isinstance(v, (int, float)):
                    info['measurement'] = "iot_point_float"
                    info['fields']['value'] = float(v)
                point_list.append(info)
        else:
            point_list.append(write_sql)
        return point_list

    def write_points(self, data):
        influxdb_data = self.set_influxdb_data(data)
        '''
        [{
            'measurement': 'iot_point_float',
            'tags': {
                'dataoid': 'def4',
                'devoid': '2b36e25d',
                'data_name': 'DO2',
                'dev_name': '6栋宿舍-2'
            },
            'time': '2022-11-23T05:33:51.564017',
            'fields': {
                'value': 0.0
            }
        }]
        '''
        self.influx.write_points(influxdb_data)
        return None

    # 查看最近一条数据
    # epoch: 'h', 'm', 's', 'ms', 'u', or 'ns', None, defaults to `s`
    def get_last_data(self, dataoid, devoid, epoch="ms", val_type=None):
        res = {}
        dataoid_info = re.match(r'[A-Za-z0-9 ]+$', dataoid)
        devoid_info = re.match(r'[A-Za-z0-9 ]+$', devoid)
        try:
            if dataoid_info and devoid_info:
                if val_type == 'INT' or val_type == 'BOOL':
                    result = self.influx.query(
                        "select last(value) from iot_point_float where dataoid='{}' and devoid='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid
                        ), epoch=epoch)

                    if result:
                        for info in result.items()[0][1]:
                            res["value"] = int(info["last"])
                            res["time"] =  info["time"]
                elif val_type == 'FLOAT':
                    result = self.influx.query(
                        "select last(value) from iot_point_float where dataoid='{}' and devoid='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid
                        ), epoch=epoch)
                    if result:
                        for info in result.items()[0][1]:
                            res["value"] = info["last"]
                            res["time"] = info["time"]
                # elif val_type == 'JSON':
                #     pass
                else:
                    result = self.influx.query(
                        "select last(value) from iot_point where dataoid='{}' and devoid='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid
                        ), epoch=epoch)

                    if result:
                        for info in result.items()[0][1]:
                            res["value"] = info["last"]
                            res["time"] = info["time"]
                return res
            else:
                return res
        except Exception as e:
            traceback.print_exc()
            return res


    # 查询时间段数据
    def get_all_data(self, dataoid, devoid, first_time, last_time, epoch="ms", val_type=None):
        res = []
        dataoid_info = re.match(r'[A-Za-z0-9 ]+$', dataoid)
        devoid_info = re.match(r'[A-Za-z0-9 ]+$', devoid)
        try:
            if dataoid_info and devoid_info:
                if val_type == 'INT' or val_type == 'BOOL':
                    results = self.influx.query(
                        "select value from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' and time>='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time
                        ), epoch=epoch)
                    if results:
                        for info in results.items()[0][1]:
                            res.append({
                                "value": int(info['value']),
                                "time": info['time']
                            })
                elif val_type == 'FLOAT':
                    results = self.influx.query(
                        "select value from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' and time>='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time
                        ), epoch=epoch)
                    if results:
                        for info in results.items()[0][1]:
                            res.append({
                                "value": info['value'],
                                "time": info['time']
                            })
                else:
                    results = self.influx.query(
                        "select value from iot_point where dataoid='{}' and devoid='{}' and time<='{}' and time>='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time
                        ), epoch=epoch)
                    if results:
                        for info in results.items()[0][1]:
                            res.append({
                                "value": info['value'],
                                "time": info['time']
                            })
                return res
            else:
                return res
        except Exception as e:
            traceback.print_exc()
            return res

    # 用户指定sql查询
    def get_sql_data(self, dataoid, devoid, first_time, last_time, epoch="ms", val_type=None, sql=None):
        res = []
        dataoid_info = re.match(r'[A-Za-z0-9 ]+$', dataoid)
        devoid_info = re.match(r'[A-Za-z0-9 ]+$', devoid)

        try:
            if dataoid_info and devoid_info:
                if val_type == 'INT' or val_type == 'BOOL':
                    sql_cmd = "select value from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' " \
                          "and time>='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time
                        )
                    if sql:
                        sql_cmd = "select value from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' " \
                          "and time>='{}' and (is_parent='' or is_parent='1') and {} tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time, sql
                        )
                    results = self.influx.query(sql_cmd, epoch=epoch)
                    if results:
                        for info in results.items()[0][1]:
                            res.append({
                                "value": int(info['value']),
                                "time": info['time']
                            })
                elif val_type == 'FLOAT':
                    sql_cmd = "select value from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' " \
                              "and time>='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                        dataoid, devoid, last_time, first_time
                    )
                    if sql:
                        sql_cmd = "select value from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' " \
                                  "and time>='{}' and (is_parent='' or is_parent='1') and {} tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time, sql
                        )
                    results = self.influx.query(sql_cmd, epoch=epoch)
                    if results:
                        for info in results.items()[0][1]:
                            res.append({
                                "value": info['value'],
                                "time": info['time']
                            })
                elif val_type == 'JSON':
                    # 没有sql参数，就按照字符串个形式查询
                    sql_cmd = "select value from iot_point where dataoid='{}' and devoid='{}' and time<='{}' " \
                              "and time>='{}' and is_parent='1' tz('Asia/Shanghai')".format(
                        dataoid, devoid, last_time, first_time
                    )
                    if sql:
                        res_flag=[]
                        # 指令替换，key替换成data_name
                        sql_new = sql.replace('key', 'data_name')
                        # 先查iot_point_float表，再查iot_point，为什么呢？因为他可能是通过or联合查询，or前面的是int类型，or后面的是string类型
                        sql_cmd_1 = "select value, flag from iot_point_float where dataoid='{}' and devoid='{}' and time<='{}' " \
                                  "and time>='{}' and {} tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time, sql_new
                        )
                        res_1 = self.influx.query(sql_cmd_1, epoch=epoch)
                        if res_1:
                            for i in res_1.items()[0][1]:
                                res_flag.append(i)

                        sql_cmd_2 = "select value, flag from iot_point where dataoid='{}' and devoid='{}' and time<='{}' " \
                                    "and time>='{}' and is_parent='0' and {} tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time, sql_new
                        )
                        res_2 = self.influx.query(sql_cmd_2, epoch=epoch)
                        if res_2:
                            for i in res_2.items()[0][1]:
                                res_flag.append(i)

                        if res_flag:
                            for info in res_flag:
                                flag = info['flag']
                                # 找到了对应的flag，然后去iot_point表里面查出来对应的json数据
                                json_sql = "select value from iot_point where dataoid='{}' and devoid='{}' " \
                                           "and time<='{}' and time>='{}' and is_parent='1' and flag='{}' tz('Asia/Shanghai')".format(
                                                dataoid, devoid, last_time, first_time, flag
                                            )
                                results = self.influx.query(json_sql, epoch=epoch)
                                for i in results.items()[0][1]:
                                    res.append({
                                        "value": i['value'],
                                        "time": i['time']
                                    })

                    else:
                        results = self.influx.query(sql_cmd, epoch=epoch)
                        for info in results.items()[0][1]:
                            res.append({
                                "value": info['value'],
                                "time": info['time']
                            })

                else:
                    sql_cmd = "select value from iot_point where dataoid='{}' and devoid='{}' and time<='{}' " \
                              "and time>='{}' and (is_parent='' or is_parent='1') tz('Asia/Shanghai')".format(
                        dataoid, devoid, last_time, first_time
                    )
                    if sql:
                        sql_cmd = "select value from iot_point where dataoid='{}' and devoid='{}' and time<='{}' " \
                                  "and time>='{}' and (is_parent='' or is_parent='1') and {} tz('Asia/Shanghai')".format(
                            dataoid, devoid, last_time, first_time, sql
                        )
                    results = self.influx.query(sql_cmd, epoch=epoch)
                    if results:
                        for info in results.items()[0][1]:
                            res.append({
                                "value": info['value'],
                                "time": info['time']
                            })
                return res
            else:
                return res
        except Exception as e:
            traceback.print_exc()
            return res

    def get_influx_info(self, epoch="ms"):
        res = []
        sql = 'show retention policies on telegraf'
        influx_info = self.influx.query(sql, epoch=epoch)
        if influx_info:
            for info in influx_info.items()[0][1]:
                res.append(info)
        return res

    def alter_influx_info(self, type_value='', value='', epoch="ms"):
        sql = ''
        if type_value == 'create':
            sql = 'create retention policy  "rp_iot_point" on telegraf duration {} replication 1 default'.format(value)
        elif type_value == 'update':
            sql = 'ALTER retention policy  "rp_iot_point" on telegraf duration {}'.format(value)
        res = []

        influx_info = self.influx.query(sql, epoch=epoch)
        if influx_info:
            for info in influx_info.items()[0][1]:
                res.append(info)
        return res