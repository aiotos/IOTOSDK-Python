#coding=utf-8
import sys
sys.path.append("..")
from driver import *
import time
import datetime
from urllib import urlencode
import urllib2
import base64
import hmac
import json
from hashlib import sha1
reload(sys)
sys.setdefaultencoding('utf8')

#签名算法
def signature(key, application, timestamp, param, body):
    code = "application:" + application + "\n" + "timestamp:" + timestamp + "\n"
    for v in param:
        code += str(v[0]) + ":" + str(v[1]) + "\n"
    if (body is not None) and (body.strip()) :
        code += body + '\n'
    # print("param=" + str(param))
    # print("body=" + str(body))
    # print("code=" + str(code))
    return base64.b64encode(hash_hmac(key, code, sha1))

def hash_hmac(key, code, sha1):
    hmac_code = hmac.new(key.encode(), code.encode(), sha1)
    print("hmac_code=" + str(hmac_code.hexdigest()))
    return hmac_code.digest()


def getTimeOffset(url):
    request = urllib2.Request(url)
    start = int(time.time() * 1000)
    response = urllib2.urlopen(request)
    end = int(time.time() * 1000)

    if response is not None:
        return int(int(response.headers['x-ag-timestamp']) - (end + start) / 2);
    else:
        return 0

baseUrl = 'https://ag-api.ctwing.cn'
timeUrl = 'https://ag-api.ctwing.cn/echo'
offset = getTimeOffset(timeUrl)

#发送http请求函数
def sendSDKRequest(path, head, param, body, version, application, MasterKey, key, method=None, isNeedSort=True,isNeedGetTimeOffset=False):
    paramList = []
    for key_value in param:
        paramList.append([key_value, param[key_value]])
    print("paramList=" + str(paramList))
    if (MasterKey is not None) and (MasterKey.strip()):
        paramList.append(['MasterKey', MasterKey])
    if isNeedSort:
        paramList = sorted(paramList)

    headers = {}
    if (MasterKey is not None) and (MasterKey.strip()):
        headers['MasterKey'] = MasterKey
    headers['application'] = application
    headers['Date'] = str(datetime.datetime.now())
    headers['version'] = version
    # headers['Content-Type'] = Content_Type
    # headers['sdk'] = sdk
    # headers['Accept'] = Accept
    # headers['User-Agent'] = User_Agent
    # headers['Accept-Encoding'] = 'gzip,deflate'

    temp = dict(param.items())
    if (MasterKey is not None) and (MasterKey.strip()):
        temp['MasterKey'] = MasterKey

    url_params = urlencode(temp)

    url = baseUrl + path
    if (url_params is not None) and (url_params.strip()):
        url = url + '?' + url_params
    print("url=" + str(url))
    global offset
    if isNeedGetTimeOffset:
        offset = getTimeOffset(timeUrl)
    timestamp = str(int(time.time() * 1000) + offset)
    headers['timestamp'] = timestamp
    sign = signature(key, application, timestamp, paramList, body)
    headers['signature'] = sign

    headers.update(head)

    print("headers : %s" % (str(headers)))

    if (body is not None) and (body.strip()):
        request = urllib2.Request(url=url, headers=headers, data=body.encode('utf-8'))
    else:
        request = urllib2.Request(url=url, headers=headers)
    if (method is not None):
        request.get_method = lambda: method
    response = urllib2.urlopen(request)
    if ('response' in vars()):
        print("response.code: %d" % (response.code))
        return response
    else:
        return None

#查询单个设备最新状态
def QueryDeviceStatus(appKey, appSecret, body):
    path = '/aep_device_status/deviceStatus'
    head = {}
    param = {}
    version = '20181031202028'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, None, key, 'POST')
    if response is not None:
        return response.read()
    return None

#分页查询设备历史数据
def getDeviceStatusHisInPage(appKey, appSecret, body):
    path = '/aep_device_status/getDeviceStatusHisInPage'
    head = {}
    param = {}
    version = '20190928013337'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, None, key, 'POST')
    if response is not None:
        return response.read()
    return None

#数据下发
def CreateCommand(appKey, appSecret, MasterKey, body):
    path = '/aep_device_command/command'
    head = {}
    param = {}
    version = '20190712225145'
    application = appKey
    key = appSecret
    response = sendSDKRequest(path, head, param, body, version, application, MasterKey, key, 'POST')
    if response is not None:
        return response.read()
    return None

class Project(IOTOSDriverI):
    def InitComm(self,attrs):
        self.setPauseCollect(False)
        self.setCollectingOneCircle(False)
        self.online(True)

        try:
            #获取中台配置的参数（必不可少）
            self.Nbapplication = self.sysAttrs['config']['param']['application']  # APPKEY
            self.Nbkey = self.sysAttrs['config']['param']['key']  # APPScret
            self.NbMasterKey=self.sysAttrs['config']['param']['MasterKey']  #MasterKey
            # self.NbMasterKey = "5fef44837aa54f42a7a49b73a4d95a15"
            self.NbproductId = self.sysAttrs['config']['param']['productId']
            self.NbdeviceId = self.sysAttrs['config']['param']['deviceId']
        except Exception,e:
            self.debug(u'获取参数失败！'+e.message)

    def Collecting(self, dataId):
        # application = "7CL0z7LNs14"  # APPKEY
        # key = "GkfEquj75z"  # APPScret
        # productId = "15046289"
        # deviceId = "e612d1428a6345778c71e587bfaa5d30"

        timearry = (datetime.datetime.now() + datetime.timedelta(days=-29)).timetuple()  # 当前时间减去29天后转化为timetuple的格式用于转换成timestamp格式
        begin_timestamp=str(int(time.mktime(timearry)*1000)+offset)
        end_timestamp=str(int(time.time() * 1000) + offset)
        page_size = "1"
        page_timestamp = ""
        body_HisInPage = '{"productId":"' + self.NbproductId + '","deviceId":"' + self.NbdeviceId + '","begin_timestamp":"' + begin_timestamp + '","end_timestamp":"' + end_timestamp + '","page_size":' + page_size + ',"page_timestamp":"' + page_timestamp + '"}'

        #下发指令时请求平台的API会出现短暂的错误，利用try except跳过这个bug时间
        try:
            res = getDeviceStatusHisInPage(self.Nbapplication, self.Nbkey, body_HisInPage)
            r = eval(res)
            # self.debug(r)
            result = eval(base64.b64decode(r["deviceStatusList"][0]["APPdata"]))
        except Exception,e:
            pass

        try:
            self.setValue(u'供电电压', result["supply_voltage"])
            self.setValue(u'供电电流', result["supply_current"])
            self.setValue(u'供电功率', result["supply_power"])
            self.setValue(u'累计电量', result["power_total"])
            self.setValue(u'服务标识ID', result["serviceId"])
            self.setValue(u'时间', time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(result["dtflag"])))
            # 判断alarm是否在result里
            if "alarm" in result:
                self.debug(type(bin(result['alarm'])))
                self.setValue(u'告警',bin(result['alarm'])[2::])

            #开关赋值》》》》》》》》》》防止页面刷新而导致开关的值消失造成不能下发数据，利用memoryvalue判断是否有数据上传以及上一次上传的数据是多少，下同
            if "memoryvalue" not in self.data2attrs['a985']:
                self.setValue(u'开关控制', 1)
                # self.debug(self.data2attrs['a985'])
            else:
                self.setValue(u'开关控制', self.data2attrs['a985']["memoryvalue"])

            #定时关机时间赋值
            if "memoryvalue" not in self.data2attrs['b0dd']:
                self.setValue(u'定时关机时间', 0)
            else:
                self.setValue(u'定时关机时间', self.data2attrs['b0dd']["memoryvalue"])

            # 定时关机时间赋值
            if "memoryvalue" not in self.data2attrs['df31']:
                self.setValue(u'定时开机时间', 0)
            else:
                self.setValue(u'定时开机时间', self.data2attrs['df31']["memoryvalue"])

            # 过功用电时长赋值
            if "memoryvalue" not in self.data2attrs['f6e0']:
                self.setValue(u'过功用电时长', 0)
            else:
                self.setValue(u'过功用电时长', self.data2attrs['f6e0']["memoryvalue"])

            # 过温阈值赋值
            if "memoryvalue" not in self.data2attrs['970a']:
                self.setValue(u'过温阈值', 0)
            else:
                self.setValue(u'过温阈值', self.data2attrs['970a']["memoryvalue"])

            # 过功阈值赋值
            if "memoryvalue" not in self.data2attrs['c48f']:
                self.setValue(u'过功阈值', 0)
            else:
                self.setValue(u'过功阈值', self.data2attrs['c48f']["memoryvalue"])

            # 过压阈值赋值
            if "memoryvalue" not in self.data2attrs['d55c']:
                self.setValue(u'过压阈值', 0)
                # self.debug(self.data2attrs['d55c'])
            else:
                self.setValue(u'过压阈值', self.data2attrs['d55c']["memoryvalue"])

            # 过载电流阈值赋值
            if "memoryvalue" not in self.data2attrs['2d82']:
                self.setValue(u'过载电流阈值', 0)
            else:
                self.setValue(u'过载电流阈值', self.data2attrs['2d82']["memoryvalue"])

            # self.debug(self.data2attrs['a985']["memoryvalue"])
            # self.debug(self.data2attrs['a985'])

            self.debug(u'数据上传成功')
        except Exception, e:
            self.debug(u'数据上传失败' + e.message)

        time.sleep(15)
        return ()

    def Event_setData(self, dataId, value):
        if dataId == "a985":
            data_tmp = ""
            if value == 1:
                self.setValue(u'开关控制', 1)
                data_tmp = "true"
            else:
                self.setValue(u'开关控制', 0)
                data_tmp = "false"
            dic_tmp = '{"serviceId": 8004, "switch_control": ' + data_tmp + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId": 8004, "switch_control": '+data_tmp+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 定时关机时间 - b0dd - 8006 - close_relay_time
        elif dataId == "b0dd":
            self.setValue(u'定时关机时间', value)
            data_tmp = value
            dic_tmp = '{"serviceId": 8006, "close_relay_time": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId": 8006, "close_relay_time": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 定时开机时间 - df31 - 8005 - open_relay_time
        elif dataId == "df31":
            self.setValue(u'定时开机时间', value)
            data_tmp = value
            dic_tmp = '{"serviceId":8005,"open_relay_time": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId":8005,"open_relay_time": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 过功用电时长 - f6e0 - 8010 - overload_powertime
        elif dataId == "f6e0":
            self.setValue(u'过功用电时长', value)
            data_tmp = value
            dic_tmp = '{"serviceId":8010,"overload_powertime": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId":8010,"overload_powertime": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 过温阈值 - 970a - 8009 - over_temperature
        elif dataId == "970a":
            self.setValue(u'过温阈值', value)
            data_tmp = value
            dic_tmp = '{"serviceId":8009,"over_temperature": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId":8009,"over_temperature": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 过功阈值 - c48f - 8008 - overload_power
        elif dataId == "c48f":
            self.setValue(u'过功阈值', value)
            data_tmp = value
            dic_tmp = '{"serviceId":8008,"overload_power": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId":8008,"overload_power": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 过压阈值 - d55c - 8007 - overload_voltage
        elif dataId == "d55c":
            self.setValue(u'过压阈值', value)
            data_tmp = value
            dic_tmp = '{"serviceId":8007,"overload_voltage": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId":8007,"overload_voltage": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # 过载电流阈值 - 2d82 - 8003 - overload_current
        elif dataId == "2d82":
            self.setValue(u'过载电流阈值', value)
            data_tmp = value
            dic_tmp = '{"serviceId":8003,"overload_current": ' + str(data_tmp) + '}'
            # body = '{"content": {"dataType":1,"payload":{"serviceId":8003,"overload_current": '+str(data_tmp)+'}},"deviceId": "'+self.NbdeviceId+'","operator": "sunghoonhr","productId": "'+self.NbproductId+'","ttl": 7200,"level": 1}'
        # else:
        #     return json.dumps({'code': 404, 'msg': '', 'data': ''})
        body = '{"content": {"dataType":1,"payload":' + dic_tmp + '},"deviceId": "' + self.NbdeviceId + '","operator": "sunghoonhr","productId": "' + self.NbproductId + '","ttl": 7200,"level": 1}'
        res = CreateCommand(self.Nbapplication, self.Nbkey, self.NbMasterKey, body)
        r = eval(res)
        self.setValue(u'响应结果',r['code'])
        self.debug(res)

        return json.dumps({'code': 0, 'msg': '', 'data': ''})