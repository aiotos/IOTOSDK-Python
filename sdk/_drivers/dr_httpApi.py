# coding=utf-8
"""上海航征 SL651 TCP 3881"""
import os
import sys

sys.path.append(os.getcwd())
from driver import *

logger.setLevel(logging.DEBUG)
from rpc_service import RPCService
import logging
from http_api import DriverHttpApi, HttpCallBackI, ApiConfig

logging.getLogger(name='iotos_sdk').setLevel(logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class HttpApiDriver(IOTOSDriverI, HttpCallBackI):
    # 1、通信初始化
    __httpCallBack = None  # type: DriverHttpApi
    __rpcService = None  # type: RPCService

    def InitComm(self, attrs):
        self.setPauseCollect(True)
        self.setCollectingOneCircle(False)
        self.online(True)

        apiConfig = ApiConfig(gateway_uuid=attrs['gateway_uuid'], device_oid=self.sysId,
                              host=self.zm.http_host, jwt_token=self.zm.jwt_token)
        self.__httpCallBack = DriverHttpApi(apiConfig=apiConfig, callBackI=self)
        self.__rpcService = RPCService(rpc=self.zm.iceService, table=self.zm.m_table)
        # res = rpc.publish(attrs['name'], value_map={
        #     u'电能': int(time.time()),
        #     u'用水量': time.time()
        # })
        # logger.debug(res)

    def on_setValues(self, data):
        _uuid, dev_oid = data['id'].split('.')
        publish_data = data['data'] # type: dict
        device_name = self.__rpcService.get_device_name(oid=dev_oid)
        res = self.__rpcService.publish(device_name=device_name, value_map=publish_data)
        logger.info(res)
        return res

    def on_setValue(self, data):
        _uuid, dev_oid, data_oid = data['id'].split('.')
        ts = data['time']
        value = data['value']
        value_map = {data_oid:{
            'value': str(value),
            'timestamp': ts
        }}
        res = self.__rpcService.publish_with_oid(id=data['id'], value_map=value_map)
        logger.info(res)
        return res

    # 2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
    def Collecting(self, dataId):
        '''*************************************************
        TODO
        **************************************************'''
        return ()

    # 3、控制
    # 广播事件回调，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 4、查询
    # 查询事件回调，数据点查询访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 5、控制事件回调，数据点控制访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 6、本地事件回调，数据点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
