#!coding:utf8
from requests import Session
import requests
import time
import threading
from driver import *
from paho.mqtt.client import MQTTMessage, MQTTMessageInfo
import paho.mqtt.client as mqtt
import json
import sys

sys.path.append("..")
from mqtt.iotos_shadow import ShadowTopic, Shadow, MqttClient

class Driver(IOTOSDriverI):
    __mqttClient = None
    __shadowTopic = None
    __pointList = []
    __topicList = [] #240418，每个数据点如果不按照全局id为topic时，可以按照高级中的param下"topic"字段的自定义主题topic

    # 1、通信初始化
    def InitComm(self, attrs):

        self.online(True)        
        t = threading.Thread(target=self.mqtt_run,
                             args=(attrs,), name='mqtt-server')
        t.start()

    def name_to_uuid(self, name):
        data_oid = self.id(name)
        return self.zm.uuid + '.' + self.sysId + '.' + data_oid

    @property
    def mqttClient(self):
        return self.__mqttClient

    @property
    def shadowTopic(self):
        return self.__shadowTopic

    '''tips 240418，驱动配置可以省去所有，默认连上sys.aiotos.net:1883，可以保持默认空param:{}。注意，如果默认不填ip
    那么用的就是iotos平台内置的mqtt服务器，否则，用的就是第三方指定的mqtt服务器！结合数据点高级配置，可以指定订阅第三方
    mqtt服务器中的任意topic主题！
    {
        "driver": "python/dr_mqtt",
        "param": {
            "ip":"sys.aiotos.net",
            "port":1883   
            "username": "admin",
            "password": "public"
        }
    }
    '''
    '''240418，数据点的配置可以在高级中param内配置topic字段，这样兼容通过数据点全ID和高级配置的topic字段来配置topic
    {
        "_comment": "",
        "parentId": null,
        "disabled": false,
        "proxy": {
            "pointer": null,
            "index": -1
        },
        "param": {
            "topic":"/sys/WG583LL0722090702004/up"
        }
    }
    '''
    def mqtt_run(self, attrs):
        ionode_uuid = attrs['gateway_uuid']
        device_oid = attrs['device_oid']
        client_id = 'device_' + ionode_uuid + '_' + device_oid

        self.logger.info(('clientId', client_id))
        self.__mqttClient = MqttClient(client_id=client_id)
        
        #240418，用户名密码
        usernametmp = "admin"
        passwordtmp = 'public'
        if "username" in attrs['config']['param'].keys():
            usernametmp = attrs['config']['param']['username']
        if "password" in attrs['config']['param'].keys():  
            passwordtmp = attrs['config']['param']['password']
        self.__mqttClient.username_pw_set(usernametmp, passwordtmp)
        
        iptmp = "sys.aiotos.net"
        porttmp = 1883
        if "ip" in attrs['config']['param'].keys():
            iptmp = attrs['config']['param']['ip']
        if "port" in attrs['config']['param'].keys():   #240418，支持传入port，此前默认固定1883
            porttmp = attrs['config']['param']['port']
        self.__mqttClient.connect(iptmp, porttmp, 60)  # 600为keepalive的时间间隔
        self.__shadowTopic = ShadowTopic(ionode_uuid + '/' + device_oid)
        self.__mqttClient.on_connect  = self.mqtt_on_connect 
        self.__mqttClient.on_disconnect = self.mqtt_on_disconnect
        self.__mqttClient.on_log = self.mqtt_on_log
        self.__mqttClient.loop_start()  # 保持连接

    def mqtt_on_log(self, client, userdata, level, buf):
        # self.logger.info((client, userdata, level, buf))
        pass

    def mqtt_on_connect(self,client, userdata, flag, rc):
        if rc == 0:
            # 连接成功
            self.warn("Connection successful")
            self.mqttClient.subscribe(self.shadowTopic.update, qos=0)
            self.warn(self.shadowTopic.update)
            self.mqttClient.subscribe(self.shadowTopic.get, qos=0)
            self.warn(self.shadowTopic.get)

            #初始遍历一次，实现逐个数据点订阅
            self.setPauseCollect(False)
            self.setCollectingOneCircle(True)
        
            self.mqttClient.on_message = self.mqtt_on_message
            self.mqttClient.on_subscribe = self.mqtt_on_subscribe
            self.mqttClient.on_unsubscribe = self.mqtt_on_unsubscribe
        
        elif rc == 1:
            # 协议版本错误
            self.warn("Protocol version error")
        elif rc == 2:
            # 无效的客户端标识
            self.warn("Invalid client identity")
        elif rc == 3:
            # 服务器无法使用
            self.warn("server unavailable")
        elif rc == 4:
            # 错误的用户名或密码
            self.warn("Wrong user name or password")
        elif rc == 5:
            # 未经授权
            self.warn("unaccredited")
        self.warn("Connect with the result code " + str(rc)) 
        
    def mqtt_on_disconnect(self,client, userdata, rc):
        # rc == 0回调被调用以响应disconnect（）调用
        # 如果以任何其他值断开连接是意外的，例如可能出现网络错误。
        if rc != 0:
            self.warn("Unexpected disconnection %s" % rc) 
        self.error('MQTT Disconnected!') 
        # self.__mqttClient.connect('sys.aiotos.net', 1883, 60)  # 600为keepalive的时间间隔
        self.__mqttClient.reconnect();
            
    def mqtt_on_subscribe(self,client, userdata, mid, granted_qos):
        self.warn("on_Subscribed: 订阅成功" + str(mid) + " " + str(granted_qos))
        
    def mqtt_on_unsubscribe(self,client, userdata, mid):
        self.warn("on_unsubscribe, mid: " + str(mid)) 

    def mqtt_on_message(self, client, userdata, message):
        # self.logger.info((message.qos, message.topic, json.dumps(message.payload)))
        self.logger.info(client.client_id)
        self.logger.info(message.topic)
        self.warn(message.payload)
        # self.logger.info(json.dumps(message.payload,indent=3))

        if message.topic == self.shadowTopic.get:
            new_data = {}

            for data_oid, data in self.data2attrs.items():
                uuid = self.zm.uuid + '.' + self.sysId + '.' + data_oid
                res = self.zm.GetPlatformData(uuid)
                res = json.loads(res)
                new_data.setdefault(
                    data.get('name'), res.get('data').get('value'))
                self.logger.info(res)
            showad = {
                "state": {
                    "reported": new_data
                }
            }
            res = self.mqttClient.publish(self.shadowTopic.get_accepted, payload=json.dumps(showad))
            self.logger.info((res))

        elif self.shadowTopic.update == message.topic:
            payload = json.loads(message.payload)
            # shadow = Shadow(**payload)
            device = dict()
            if client.client_id[0:7] == 'device_':
                device = payload['state']['reported']
            else:
                self.logger.info('not device report???')
                raise SyntaxWarning('由设备负责响应处理字段')
                device = payload['state']['desired']
            if len(device) == 0:
                pass
            elif len(device) == 1:
                key, value = device.items()[0]
                res = self.setValue(name=key, value=value)
                self.logger.info(res)
            else:
                value_list = []  # 要批量上报的值结构，其中返回的值元组中第一个就是采集点自身的值，所以先append走一个！
                for key, value in device.items():
                    value_list.append(
                        {'id': self.name_to_uuid(key), 'value': value})
                res = self.setValues(value_list)
                self.logger.info(res)
                
        #240418，主题订阅如果是第三方mqtt broker，而且在数据点的高级配置param中有配置topic，那么就用上
        elif message.topic in self.__topicList and self.__topicList.index(message.topic) != -1:
            self.warn(message.topic + ':' + str(message.payload))
            dataPointTmp = self.__pointList[self.__topicList.index(message.topic)] #240418
            dataIdTmp = dataPointTmp.split('.')[2]
            try:
                self.warn(self.setValue(self.name(dataIdTmp), self.valueTyped(dataIdTmp,str(message.payload, encoding='utf-8'))))
            except Exception as e:
                try:
                    self.warn(self.setValue(self.name(dataIdTmp), self.valueTyped(dataIdTmp,message.payload.decode('utf-8'))))
                except Exception as e:
                    try:
                        self.warn(self.setValue(self.name(dataIdTmp), self.valueTyped(dataIdTmp,str(message.payload))))
                    except Exception as e:
                        self.error(e)
                        
        elif message.topic in self.__pointList and self.__pointList.index(message.topic) != -1:
            self.warn(message.topic + ':' + str(message.payload))
            dataIdTmp = message.topic.split('.')[2]
            try:
                self.warn(self.setValue(self.name(dataIdTmp), self.valueTyped(dataIdTmp,str(message.payload, encoding='utf-8'))))
            except Exception as e:
                try:
                    self.warn(self.setValue(self.name(dataIdTmp), self.valueTyped(dataIdTmp,message.payload.decode('utf-8'))))
                except Exception as e:
                    try:
                        self.warn(self.setValue(self.name(dataIdTmp), self.valueTyped(dataIdTmp,str(message.payload))))
                    except Exception as e:
                        self.error(e)
    # 2、采集
    def Collecting(self, dataId):
        
        pointtmp = self.pointId(dataId)
        self.mqttClient.subscribe(pointtmp, qos=0)
        self.__pointList.append(pointtmp)
        
        #240418，topic如果再高级配置中有自定义配置，那么也加入自动订阅列表
        cfgtmp = self.data2attrs[dataId]['config']    
        
        self.error('----------------')
        self.error(cfgtmp)
        
        topictmp = None
        if 'param' in cfgtmp.keys() and 'topic' in cfgtmp['param']:
            topictmp = cfgtmp['param']['topic']
            
            self.error('++++++++++++++++++')
            self.error(topictmp)
            
            self.mqttClient.subscribe(topictmp, qos=0)
        self.__topicList.append(topictmp) #240418，放到外面，这样不论是否有配置，那么根据索引来对应self.__pointList，知道topic主题是哪个数据点！
        
        return ()

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO

        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************

        TODO

        **************************************************'''
        data = None
        return json.dumps({'code': 0, 'msg': '', 'data': data})

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):

        # winsound.Beep(500,100)
        res = self.mqttClient.publish(self.pointId(dataId), payload=value)
        self.logger.info((res))

        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):

        return json.dumps({'code': 0, 'msg': '', 'data': ''})
