# coding=utf-8
"""设备通信管理服务"""
import time

from routelib.ice_connent import IceService
from library.dto import json_dumps


class RPCService(object):
    __rpc = None  # type: IceService
    __table = None  # type: dict
    __device_name_map = None  # type: dict[str, str]
    __data_name_map = None  # type: dict[str, str]

    __dev_oid_map = None  # type: dict[str, str]

    def __init__(self, table, rpc):
        """
        @type table: dict
        @type rpc: IceService
        """
        self.__rpc = rpc
        self.__table = table

        # 处理设备映谢
        self.__device_name_map = dict()
        self.__data_name_map = dict()
        device_dict = table['properties']  # type: dict
        for k, device in device_dict.items():  # type: str,dict
            device_name = device['name']
            self.__device_name_map[device_name] = k
            data_name_map = dict()
            data_dict = device['data']  # type: dict
            for oid, data in data_dict.items():
                data_name = data['name']
                data_name_map[data_name] = oid
            self.__data_name_map[device_name] = data_name_map

    def get_device_name(self, oid):
        """通过OID获取设备名称"""
        return self.__table['properties'][oid]['name']

    def publish(self, device_name, value_map):
        """发布数据

        @type device_name: str
        @type value_map: dict[str, int|float|string|bool]
        @param device_name: 设备名称
        @param value_map: {'数据点名称': "数据点Value"}
        @rtype: dict
        @return {name, msg, code}
        """

        # res = self.__rpc.syncPubMsg()
        device_oid = self.__device_name_map[device_name]
        data_name_map = self.__data_name_map[device_name]
        data_values = dict()
        for k, v in value_map.items():
            data_oid = data_name_map[k]
            data_values[data_oid] = {
                'value': str(v),
                'timestamp': time.time()
            }

        points = {
            'id': self.__table['id'],
            'properties': {device_oid: {
                'data': data_values
            }}
        }
        points_str = json_dumps([points])
        res = self.__rpc.syncPubMsg(points_str)
        return res

    def publish_with_oid(self, id, value_map):
        """使用原始OID,发布数据

        :type id: str
        """
        __uuid, device_oid, data_oid = id.split('.')
        points = {
            'id': self.__table['id'],
            'properties': {device_oid: {
                'data': value_map
            }}
        }
        points_str = json_dumps([points])
        res = self.__rpc.syncPubMsg(points_str)
        return res
